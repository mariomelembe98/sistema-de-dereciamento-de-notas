package validation;

import app.models.Professor;
import app.models.Usuario;

import java.util.List;

public class ValidationUtils {

    public static boolean validarEmail(String email) {

        String regex = "^(.+)@(.+)$";
        return email.matches(regex);
    }

    public static boolean validarNome(String nome) {

        String regex = "^[^\\d]+$";
        return nome.matches(regex);
    }

    public static boolean isNumero(String texto) {
        return texto.matches("\\d+(\\.\\d+)?");
    }


    public static boolean emailJaExisteUser(List<Usuario> usuarios, String email, int idIgnorar) {
        for (Usuario usuario : usuarios) {
            if (usuario.getEmail().equals(email) && usuario.getId() != idIgnorar) {
                return true;
            }
        }
        return false;
    }

    public static boolean emailJaExisteProfessor(List<Professor> professores, String email, int idIgnorar) {
        for (Professor professor : professores) {
            if (professor.getEmail().equals(email) && professor.getId() != idIgnorar) {
                return true;
            }
        }
        return false;
    }
}
