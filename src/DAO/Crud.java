package DAO;

import app.models.Disciplina;
import app.models.Usuario;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public abstract class Crud<T extends Serializable> {
    private List<T> dataList;
    private String filePath;

    public Crud(String filePath) {
        this.filePath = filePath;
        loadData();
    }

    // Carrega os dados do arquivo
    @SuppressWarnings("unchecked")
    private void loadData() {
        try (ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(filePath))) {
            dataList = (List<T>) inputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            // Se o arquivo ainda não existir, cria uma nova lista vazia
            dataList = new ArrayList<>();
        }
    }

    // Salva os dados no arquivo
    private void saveData() {
        try (ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(filePath))) {
            outputStream.writeObject(dataList);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Adiciona um novo item
    public void create(T item) {
        dataList.add(item);
        saveData();
    }

    // Retorna todos os itens
    public List<T> readAll() {
        return new ArrayList<>(dataList); // Retorna uma cópia da lista para evitar modificações indesejadas
    }

    // Atualiza um item
    public void update(int index, T newItem) {
        if (index >= 0 && index < dataList.size()) {
            dataList.set(index, newItem);
            saveData();
        } else {
            System.err.println("Índice inválido para atualização.");
        }
    }

    // Remove um item
    public void delete(int index) {
        if (index >= 0 && index < dataList.size()) {
            dataList.remove(index);
            saveData();
        } else {
            System.err.println("Índice inválido para remoção.");
        }
    }

    // Método auxiliar para obter o índice de um item por id
    protected int getIndexById(int id) {
        for (int i = 0; i < dataList.size(); i++) {
            if (((Usuario) dataList.get(i)).getId() == id) {
                return i;
            }
        }
        return -1; // Retorna -1 se não encontrar
    }

    //public abstract void update(Usuario usuario);

    public abstract void update(Disciplina disciplina);

    public abstract void updates(Usuario usuario);

    public abstract void update(Usuario usuario);

    // Corrigindo o erro: removendo o método update(Usuario)
    //public abstract void update(Usuario usuario);
}
