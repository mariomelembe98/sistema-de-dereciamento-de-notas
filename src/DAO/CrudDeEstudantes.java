package DAO;

import app.models.Estudante;
import raven.toast.Notifications;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by USER on 16/05/2024.
 */

public class CrudDeEstudantes {
    private List<Estudante> dataList;
    private String filePath;

    public CrudDeEstudantes(String filePath) {
        this.filePath = filePath;
        loadData();
    }

    // Carrega os dados do arquivo
    private void loadData() {
        dataList = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String line;
            Estudante estudante = null;
            while ((line = reader.readLine()) != null) {
                if (line.startsWith("id:")) {
                    if (estudante != null) {
                        dataList.add(estudante);
                    }
                    estudante = new Estudante();
                    try {
                        estudante.setId(Integer.parseInt(line.substring(3).trim()));
                    } catch (NumberFormatException e) {
                        System.err.println("Erro ao ler o ID: " + line);
                    }
                } else if (line.startsWith("Nome:")) {
                    if (estudante != null) estudante.setNome(line.substring(5).trim());
                }else if (line.startsWith("Codico:")) {
                    if (estudante != null) estudante.setCodigo(line.substring(8).trim());
                } else if (line.startsWith("Apelido:")) {
                    if (estudante != null) estudante.setSobrenome(line.substring(8).trim());
                } else if (line.startsWith("Data de Nascimento:")) {
                    if (estudante != null) estudante.setDataNascimento(line.substring(19).trim());
                } else if (line.startsWith("Sexo:")) {
                    if (estudante != null) estudante.setSexo(line.substring(5).trim());
                } else if (line.startsWith("Email:")) {
                    if (estudante != null) estudante.setEmail(line.substring(6).trim());
                } else if (line.startsWith("Contacto:")) {
                    if (estudante != null) estudante.setTelefone(line.substring(9).trim());
                } else if (line.startsWith("Turno:")) {
                    if (estudante != null) estudante.setTurno(line.substring(6).trim());
                } else if (line.startsWith("Nivel")) {
                    if (estudante != null) estudante.setNivel(line.substring(6).trim());
                } else if (line.startsWith("Curso:")) {
                    if (estudante != null) estudante.setCurso(line.substring(7).trim());
                } else if (line.startsWith("Nome de Usuario:")) {
                    if (estudante != null) estudante.setNomeUsuario(line.substring(16).trim());
                } else if (line.startsWith("Senha:")) {
                    if (estudante != null) estudante.setSenha(line.substring(6).trim());
                } else if (line.startsWith("Administrador:")) {
                    if (estudante != null) estudante.setAdmin(Boolean.parseBoolean(line.substring(14).trim()));
                } else if (line.startsWith("Status:")) {
                    if (estudante != null) {
                        estudante.setStatus(Boolean.parseBoolean(line.substring(7).trim()));
                    }
                }
            }
            if (estudante != null) {
                dataList.add(estudante);
            }
        } catch (IOException e) {
            System.err.println("Erro ao ler o arquivo: " + e.getMessage());
        }
    }


    // Salva os dados no arquivo
    private void saveData() {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filePath))) {
            for (Estudante estudante : dataList) {
                writer.write("id: " + estudante.getId() + "\n");
                writer.write("Codico: " + estudante.getCodigo() + "\n");
                writer.write("Nome: " + estudante.getNome() + "\n");
                writer.write("Apelido: " + estudante.getSobrenome() + "\n");
                writer.write("Data de Nascimento: " + estudante.getDataNascimento() + "\n");
                writer.write("Sexo: " + estudante.getSexo() + "\n");
                writer.write("Email: " + estudante.getEmail() + "\n");
                writer.write("Contacto: " + estudante.getTelefone() + "\n");
                writer.write("Turno: " + estudante.getTurno() + "\n");
                writer.write("Curso: " + estudante.getCurso() + "\n");
                writer.write("Nivel: " + estudante.getNivel() + "\n");
                writer.write("Nome de Usuario: " + estudante.getNomeUsuario() + "\n");
                writer.write("Senha: " + estudante.getSenha() + "\n");
                writer.write("Administrador: " + estudante.isAdmin() + "\n");
                writer.write("Status: " + estudante.getStatus() + "\n");
                writer.write("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Adiciona um novo item
    public void create(Estudante item) {
        dataList.add(item);
        saveData();
    }

    // Retorna todos os itens
    public List<Estudante> readAll() {
        return new ArrayList<>(dataList); // Retorna uma cópia da lista para evitar modificações indesejadas
    }

    // Atualiza um item
    public void update(Estudante updatedprofessor) {
        for (int i = 0; i < dataList.size(); i++) {
            Estudante professor = dataList.get(i);
            if (professor.getId() == updatedprofessor.getId()) {
                dataList.set(i, updatedprofessor);
                saveData();
                return;
            }
        }
        Notifications.getInstance().show(Notifications.Type.ERROR, Notifications.Location.TOP_CENTER, "Usuário não encontrado para atualização.");
    }

    // Remove um item
    public void delete(int id) {
        dataList.removeIf(professor -> professor.getId() == id);
        saveData();
    }
}
