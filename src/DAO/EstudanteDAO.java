package DAO;

import app.models.Estudante;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class EstudanteDAO {
    private static final String FILE_PATH = "estudantes.dat";

    public static List<Estudante> readEstudantesFromFile() {
        List<Estudante> estudantes = new ArrayList<>();
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(FILE_PATH))) {
            estudantes = (List<Estudante>) ois.readObject();
        } catch (FileNotFoundException e) {
            System.out.println("Arquivo não encontrado: " + FILE_PATH);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return estudantes;
    }
}
