package DAO;

public interface LoginListener {
    void onLoginSuccess();
    void onLoginFailed();
}
