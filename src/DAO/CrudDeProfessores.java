package DAO;

import app.models.Professor;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class CrudDeProfessores {
    private List<Professor> dataList;
    private String filePath;

    public CrudDeProfessores(String filePath) {
        this.filePath = filePath;
        loadData();
    }

    private void loadData() {
        dataList = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String line;
            Professor professor = null;
            while ((line = reader.readLine()) != null) {
                if (line.startsWith("id:")) {
                    if (professor != null) {
                        dataList.add(professor);
                    }
                    professor = new Professor();
                    professor.setId(Integer.parseInt(line.substring(4).trim()));
                } else if (line.startsWith("Nome:")) {
                    professor.setNome(line.substring(6).trim());
                } else if (line.startsWith("Apelido:")) {
                    professor.setApelido(line.substring(9).trim());
                } else if (line.startsWith("Sexo:")) {
                    professor.setSexo(line.substring(6).trim());
                } else if (line.startsWith("Email:")) {
                    professor.setEmail(line.substring(7).trim());
                } else if (line.startsWith("Telefone:")) {
                    professor.setTelefone(line.substring(10).trim());
                } else if (line.startsWith("NomeUsuario:")) {
                    professor.setNomeUsuario(line.substring(13).trim());
                } else if (line.startsWith("Senha:")) {
                    professor.setSenha(line.substring(7).trim());
                } else if (line.startsWith("Status:")) {
                    professor.setStatus(Boolean.parseBoolean(line.substring(8).trim()));
                } else if (line.startsWith("IsAdmin:")) {
                    professor.setAdmin(Boolean.parseBoolean(line.substring(9).trim()));
                } else if (line.startsWith("TurmaId:")) {
                    professor.adicionarTurma(Integer.parseInt(line.substring(8).trim()));
                }
            }
            if (professor != null) {
                dataList.add(professor);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void saveData() {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filePath))) {
            for (Professor professor : dataList) {
                writer.write("id: " + professor.getId() + "\n");
                writer.write("Nome: " + professor.getNome() + "\n");
                writer.write("Apelido: " + professor.getApelido() + "\n");
                writer.write("Sexo: " + professor.getSexo() + "\n");
                writer.write("Email: " + professor.getEmail() + "\n");
                writer.write("Telefone: " + professor.getTelefone() + "\n");
                writer.write("NomeUsuario: " + professor.getNomeUsuario() + "\n");
                writer.write("Senha: " + professor.getSenha() + "\n");
                writer.write("Status: " + professor.getStatus() + "\n");
                writer.write("IsAdmin: " + professor.isAdmin() + "\n");
                for (Integer turmaId : professor.getTurmaIds()) {
                    writer.write("TurmaId: " + turmaId + "\n");
                }
                writer.write("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void create(Professor item) {
        // TODO: Verifica se o nome do professor já existe
        if (dataList.stream().anyMatch(professor -> professor.getNome().equalsIgnoreCase(item.getNome()))) {
            System.err.println("Já existe um professor com o mesmo nome. " + item.getNome());
            return; // Sai do método sem adicionar o professor
        }

        // TODO: Obtém o próximo ID disponível
        int nextId = getNextId();
        item.setId(nextId);

        // TODO: Adiciona o novo professor à lista e salva os dados
        dataList.add(item);
        saveData();
    }

    public List<Professor> readAll() {
        return new ArrayList<>(dataList); // Retorna uma cópia da lista para evitar modificações indesejadas
    }

    public List<Professor> search(Predicate<Professor> criteria) {
        return dataList.stream().filter(criteria).collect(Collectors.toList());
    }

    public Professor read(int id) {
        for (Professor professor : dataList) {
            if (professor.getId() == id) {
                return professor;
            }
        }
        return null; // ou lançar uma exceção se o professor não for encontrado
    }

    public void update(Professor updatedProfessor) {
        for (int i = 0; i < dataList.size(); i++) {
            Professor professor = dataList.get(i);
            if (professor.getId() == updatedProfessor.getId()) {
                dataList.set(i, updatedProfessor);
                saveData();
                return;
            }
        }
        System.err.println("Professor não encontrado para atualização.");
    }

    public void delete(int id) {
        dataList.removeIf(professor -> professor.getId() == id);
        saveData();
    }

    public int getNextId() {
        return dataList.stream().mapToInt(Professor::getId).max().orElse(0) + 1;
    }
}
