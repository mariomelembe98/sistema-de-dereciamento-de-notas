package DAO;

import app.models.Turma;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class CrudDeTurmas {
    private List<Turma> dataList;
    private String filePath;

    public CrudDeTurmas(String filePath) {
        this.filePath = filePath;
        loadData();
    }

    private void loadData() {
        dataList = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String line;
            Turma turma = null;
            while ((line = reader.readLine()) != null) {
                if (line.startsWith("id:")) {
                    if (turma != null) {
                        dataList.add(turma);
                    }
                    turma = new Turma();
                    turma.setId(Integer.parseInt(line.substring(4).trim()));
                } else if (line.startsWith("Nome:")) {
                    turma.setNome(line.substring(6).trim());
                } else if (line.startsWith("Ano:")) {
                    turma.setAno(line.substring(5).trim());
                } else if (line.startsWith("Semestre:")) {
                    turma.setSemestre(line.substring(10).trim());
                } else if (line.startsWith("Turno")) {
                    turma.setTurno(line.substring(7).trim());
                } else if (line.startsWith("ProfessorId:")) {
                    turma.adicionarProfessor(Integer.parseInt(line.substring(12).trim()));
                }
            }
            if (turma != null) {
                dataList.add(turma);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void saveData() {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filePath))) {
            for (Turma turma : dataList) {
                writer.write("id: " + turma.getId() + "\n");
                writer.write("Nome: " + turma.getNome() + "\n");
                writer.write("Ano: " + turma.getAno() + "\n");
                writer.write("Semestre: " + turma.getSemestre() + "\n");
                writer.write("Turno: " + turma.getTurno() + "\n");
                for (Integer professorId : turma.getProfessorIds()) {
                    writer.write("ProfessorId: " + professorId + "\n");
                }
                writer.write("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void create(Turma item) {
        // TODO: Verifica se o nome da turma já existe
        if (dataList.stream().anyMatch(turma -> turma.getNome().equalsIgnoreCase(item.getNome()))) {
            System.err.println("Já existe uma turma com o mesmo nome. " + item.getNome());
            return; // Sai do método sem adicionar a turma
        }

        // TODO: Obtém o próximo ID disponível
        int nextId = getNextId();
        item.setId(nextId);

        // TODO: Adiciona a nova turma à lista e salva os dados
        dataList.add(item);
        saveData();
    }

    public List<Turma> readAll() {
        return new ArrayList<>(dataList); // Retorna uma cópia da lista para evitar modificações indesejadas
    }

    public List<Turma> search(Predicate<Turma> criteria) {
        return dataList.stream().filter(criteria).collect(Collectors.toList());
    }

    public Turma read(int id) {
        for (Turma turma : dataList) {
            if (turma.getId() == id) {
                return turma;
            }
        }
        return null; // ou lançar uma exceção se a turma não for encontrada
    }

    public void update(Turma updatedTurma) {
        for (int i = 0; i < dataList.size(); i++) {
            Turma turma = dataList.get(i);
            if (turma.getId() == updatedTurma.getId()) {
                dataList.set(i, updatedTurma);
                saveData();
                return;
            }
        }
        System.err.println("Turma não encontrada para atualização.");
    }

    public void delete(int id) {
        dataList.removeIf(turma -> turma.getId() == id);
        saveData();
    }

    public int getNextId() {
        return dataList.stream().mapToInt(Turma::getId).max().orElse(0) + 1;
    }
}
