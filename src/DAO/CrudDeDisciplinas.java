package DAO;

import app.models.Disciplina;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class CrudDeDisciplinas {
    private List<Disciplina> dataList;
    private String filePath;

    public CrudDeDisciplinas(String filePath) {
        this.filePath = filePath;
        loadData();
    }

    private void loadData() {
        dataList = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String line;
            Disciplina disciplina = null;
            while ((line = reader.readLine()) != null) {
                if (line.startsWith("id:")) {
                    if (disciplina != null) {
                        dataList.add(disciplina);
                    }
                    disciplina = new Disciplina();
                    disciplina.setId(Integer.parseInt(line.substring(4).trim()));
                } else if (line.startsWith("Nome:")) {
                    disciplina.setNome(line.substring(6).trim());
                } else if (line.startsWith("Curso:")) {
                    disciplina.setCurso(line.substring(7).trim());
                }
            }
            if (disciplina != null) {
                dataList.add(disciplina);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void saveData() {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filePath))) {
            for (Disciplina disciplina : dataList) {
                writer.write("id: " + disciplina.getId() + "\n");
                writer.write("Nome: " + disciplina.getNome() + "\n");
                writer.write("Curso: " + disciplina.getCurso() + "\n");
                writer.write("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void create(Disciplina item) {
        if (dataList.stream().anyMatch(d -> d.getNome().equalsIgnoreCase(item.getNome()))) {
            System.err.println("Erro: O nome da disciplina já existe.");
            return;
        }
        dataList.add(item);
        saveData();
    }

    public List<Disciplina> readAll() {
        return new ArrayList<>(dataList); // Retorna uma cópia da lista para evitar modificações indesejadas
    }

    public List<Disciplina> search(Predicate<Disciplina> criteria) {
        return dataList.stream().filter(criteria).collect(Collectors.toList());
    }

    public Disciplina read(int id) {
        for (Disciplina disciplina : dataList) {
            if (disciplina.getId() == id) {
                return disciplina;
            }
        }
        return null; // ou lançar uma exceção se a disciplina não for encontrada
    }

    public void update(Disciplina updatedDisciplina) {
        for (int i = 0; i < dataList.size(); i++) {
            Disciplina disciplina = dataList.get(i);
            if (disciplina.getId() == updatedDisciplina.getId()) {
                if (dataList.stream().anyMatch(d -> d.getNome().equalsIgnoreCase(updatedDisciplina.getNome()) && d.getId() != updatedDisciplina.getId())) {
                    System.err.println("Erro: O nome da disciplina já existe.");
                    return;
                }
                dataList.set(i, updatedDisciplina);
                saveData();
                return;
            }
        }
        System.err.println("Disciplina não encontrada para atualização.");
    }

    public void delete(int id) {
        dataList.removeIf(disciplina -> disciplina.getId() == id);
        saveData();
    }

    public int getNextId() {
        return dataList.stream().mapToInt(Disciplina::getId).max().orElse(0) + 1;
    }
}
