package DAO;

import app.models.Nota;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class CrudDeNotas {
    private List<Nota> dataList;
    private String filePath;

    public CrudDeNotas(String filePath) {
        this.filePath = filePath;
        loadData();
    }

    private void loadData() {
        dataList = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String line;
            Nota nota = null;
            while ((line = reader.readLine()) != null) {
                if (line.startsWith("id:")) {
                    if (nota != null) {
                        dataList.add(nota);
                    }
                    nota = new Nota();
                    nota.setId(Integer.parseInt(line.substring(4).trim()));
                } else if (line.startsWith("turmaId:")) {
                    nota.setTurmaId(Integer.parseInt(line.substring(8).trim()));
                } else if (line.startsWith("estudanteId:")) {
                    nota.setEstudanteId(Integer.parseInt(line.substring(12).trim()));
                } else if (line.startsWith("disciplinaId:")) {
                    nota.setDisciplinaId(Integer.parseInt(line.substring(13).trim()));
                } else if (line.startsWith("nota1:")) {
                    nota.setNota1(Double.parseDouble(line.substring(7).trim()));
                } else if (line.startsWith("nota2:")) {
                    nota.setNota2(Double.parseDouble(line.substring(7).trim()));
                } else if (line.startsWith("media:")) {
                    nota.setMedia(Double.parseDouble(line.substring(7).trim()));
                }
            }
            if (nota != null) {
                dataList.add(nota);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void saveData() {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filePath))) {
            for (Nota nota : dataList) {
                writer.write("id: " + nota.getId() + "\n");
                writer.write("turmaId: " + nota.getTurmaId() + "\n");
                writer.write("estudanteId: " + nota.getEstudanteId() + "\n");
                writer.write("disciplinaId: " + nota.getDisciplinaId() + "\n");
                writer.write("nota1: " + nota.getNota1() + "\n");
                writer.write("nota2: " + nota.getNota2() + "\n");
                writer.write("media: " + nota.getMedia() + "\n");
                writer.write("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void create(Nota item) {
        int nextId = getNextId();
        item.setId(nextId);
        dataList.add(item);
        saveData();
    }

    public List<Nota> readAll() {
        return new ArrayList<>(dataList);
    }

    public List<Nota> search(Predicate<Nota> criteria) {
        return dataList.stream().filter(criteria).collect(Collectors.toList());
    }

    public Nota read(int id) {
        for (Nota nota : dataList) {
            if (nota.getId() == id) {
                return nota;
            }
        }
        return null;
    }

    public void update(Nota updatedNota) {
        for (int i = 0; i < dataList.size(); i++) {
            Nota nota = dataList.get(i);
            if (nota.getId() == updatedNota.getId()) {
                dataList.set(i, updatedNota);
                saveData();
                return;
            }
        }
        System.err.println("Nota não encontrada para atualização.");
    }

    public void createOrUpdate(Nota nota) {
        for (int i = 0; i < dataList.size(); i++) {
            Nota existingNota = dataList.get(i);
            if (existingNota.getEstudanteId() == nota.getEstudanteId() &&
                    existingNota.getTurmaId() == nota.getTurmaId() &&
                    existingNota.getDisciplinaId() == nota.getDisciplinaId()) {
                dataList.set(i, nota);
                saveData();
                return;
            }
        }
        create(nota);
    }

    public void delete(int id) {
        dataList.removeIf(nota -> nota.getId() == id);
        saveData();
    }

    public int getNextId() {
        return dataList.stream().mapToInt(Nota::getId).max().orElse(0) + 1;
    }
}
