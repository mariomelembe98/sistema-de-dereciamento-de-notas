package DAO;

import app.models.Curso;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CrudDeCursos {
    private List<Curso> dataList;
    private String filePath;

    public CrudDeCursos(String filePath) {
        this.filePath = filePath;
        loadData();
    }

    // Carrega os dados do arquivo
    private void loadData() {
        dataList = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String line;
            Curso curso = null;
            while ((line = reader.readLine()) != null) {
                if (line.startsWith("id:")) {
                    if (curso != null) {
                        dataList.add(curso);
                    }
                    curso = new Curso();
                    curso.setId(Integer.parseInt(line.substring(4).trim()));
                } else if (line.startsWith("Nome:")) {
                    curso.setNome(line.substring(5).trim());
                } else if (line.startsWith("Descricao:")) {
                    curso.setDescricao(line.substring(10).trim());
                }
            }
            if (curso != null) {
                dataList.add(curso);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Salva os dados no arquivo
    private void saveData() {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filePath))) {
            for (Curso curso : dataList) {
                writer.write("id: " + curso.getId() + "\n");
                writer.write("Nome: " + curso.getNome() + "\n");
                writer.write("Descricao: " + curso.getDescricao() + "\n");
                writer.write("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Adiciona um novo item
    public void create(Curso item) {
        dataList.add(item);
        saveData();
    }

    // Retorna todos os itens
    public List<Curso> readAll() {
        return new ArrayList<>(dataList); // Retorna uma cópia da lista para evitar modificações indesejadas
    }

    // Atualiza um item
    public void update(Curso updatedcurso) {
        for (int i = 0; i < dataList.size(); i++) {
            Curso curso = dataList.get(i);
            if (curso.getId() == updatedcurso.getId()) {
                dataList.set(i, updatedcurso);
                saveData();
                return;
            }
        }
        System.err.println("Curso não encontrado para actualização.");
    }

    // Remove um item
    public void delete(int id) {
        dataList.removeIf(curso -> curso.getId() == id);
        saveData();
    }


}
