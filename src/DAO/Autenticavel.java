package DAO;

public interface Autenticavel {
    String getNomeUsuario();
    String getSenha();
}
