package DAO;

import app.models.Usuario;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class CrudDeUsuarios {
    private List<Usuario> dataList;
    private String filePath;

    public CrudDeUsuarios(String filePath) {
        this.filePath = filePath;
        loadData();
    }

    // TODO: Carrega os dados do arquivo
    private void loadData() {
        dataList = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String line;
            Usuario usuario = null;
            while ((line = reader.readLine()) != null) {
                if (line.startsWith("id:")) {
                    if (usuario != null) {
                        dataList.add(usuario);
                    }
                    usuario = new Usuario();
                    usuario.setId(Integer.parseInt(line.substring(4).trim()));
                } else if (line.startsWith("Nome:")) {
                    usuario.setNome(line.substring(6).trim());
                } else if (line.startsWith("Nome do Usuário:")) {
                    usuario.setNomeUsuario(line.substring(17).trim());
                } else if (line.startsWith("Email:")) {
                    usuario.setEmail(line.substring(7).trim());
                } else if (line.startsWith("Senha:")) {
                    usuario.setSenha(line.substring(7).trim());
                } else if (line.startsWith("Status:")) {
                    usuario.setStatus(Boolean.parseBoolean(line.substring(8).trim()));
                } else if (line.startsWith("Activo:")) {
                    usuario.setAdmin(Boolean.parseBoolean(line.substring(8).trim()));
                }
            }
            if (usuario != null) {
                dataList.add(usuario);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // TODO: Salva os dados no arquivo
    private void saveData() {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filePath))) {
            for (Usuario usuario : dataList) {
                writer.write("id: " + usuario.getId() + "\n");
                writer.write("Nome: " + usuario.getNome() + "\n");
                writer.write("Nome do Usuário: " + usuario.getNomeUsuario() + "\n");
                writer.write("Email: " + usuario.getEmail() + "\n");
                writer.write("Senha: " + usuario.getSenha() + "\n");
                writer.write("Status: " + usuario.getStatus() + "\n");
                writer.write("Activo: " + usuario.isAdmin() + "\n");
                writer.write("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // TODO: Adiciona um novo item
    public void create(Usuario item) {
        dataList.add(item);
        saveData();
    }

    // TODO: Retorna todos os itens
    public List<Usuario> readAll() {
        return new ArrayList<>(dataList); // Retorna uma cópia da lista para evitar modificações indesejadas
    }

    // TODO: Pesquisa usuários baseado em um critério
    public List<Usuario> search(Predicate<Usuario> criteria) {
        return dataList.stream().filter(criteria).collect(Collectors.toList());
    }


    public Usuario read(int id) {
        for (Usuario usuario : dataList) {
            if (usuario.getId() == id) {
                return usuario;
            }
        }
        return null; // ou lançar uma exceção se o usuário não for encontrado
    }


    // TODO: Actualiza um item
    public void update(Usuario updatedUsuario) {
        for (int i = 0; i < dataList.size(); i++) {
            Usuario usuario = dataList.get(i);
            if (usuario.getId() == updatedUsuario.getId()) {
                dataList.set(i, updatedUsuario);
                saveData();
                return;
            }
        }
        System.err.println("Usuário não encontrado para actualização.");
    }

    // TODO: Remove um item
    public void delete(int id) {
        dataList.removeIf(usuario -> usuario.getId() == id);
        saveData();
    }

    // TODO: Verifica se as credenciais fornecidas correspondem a um usuário existente
    public Usuario verificarCredenciais(String nomeUsuario, String senha) {
        for (Usuario usuario : dataList) {
            if (usuario.getNomeUsuario().equals(nomeUsuario) && usuario.getSenha().equals(senha)) {
                return usuario;
            }
        }
        return null;
    }
}
