package DAO;

import app.models.Turma;
import java.util.ArrayList;
import java.util.List;

public class TurmaDAO {
    private static TurmaDAO instance;
    private List<Turma> todasAsTurmas;

    private TurmaDAO() {

    }

    public static TurmaDAO getInstance() {
        if (instance == null) {
            instance = new TurmaDAO();
        }
        return instance;
    }

    public List<Turma> getTodasAsTurmas() {
        return todasAsTurmas;
    }

    public Turma getTurmaById(int id) {
        for (Turma turma : todasAsTurmas) {
            if (turma.getId() == id) {
                return turma;
            }
        }
        return null;
    }

    public void addTurma(Turma turma) {
        todasAsTurmas.add(turma);
    }
}
