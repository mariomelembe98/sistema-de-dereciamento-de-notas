package sistema;

import java.util.List;

public interface Crud<T> {
    void create(T t);
    List<T> readAll();
    void update(T t);
    void delete(int id);
}
