package views.dashboard.components;

import DAO.CrudDeProfessores;
import DAO.CrudDeTurmas;
import app.models.Professor;
import app.models.Turma;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.List;

public class PanelProfessores extends JPanel {
    private JLabel idLabel;
    private JTextField idTextField;
    private JTextField nomeField;
    private JTextField apelidoField;
    private JComboBox<String> sexoComboBox;
    private JTextField emailField;
    private JTextField telefoneField;
    private JCheckBox statusCheckBox;
    private JCheckBox isAdminCheckBox;
    private JTextField nomeUsuarioField;
    private JPasswordField passwordField;
    private JPasswordField confirmarPasswordField;
    private JList<Turma> turmasList;
    private JButton salvarButton;
    private JButton actualizarButton;
    private JButton eliminarButton;
    private JButton limparButton;
    private JTable professoresTable;
    private DefaultTableModel tableModel;

    public PanelProfessores() {
        setBackground(Color.WHITE);
        setLayout(null);

        // Campos de texto e rótulos
        JLabel tituloLabel = new JLabel("Cadastro de Professores");
        tituloLabel.setFont(new Font("Tahoma", Font.BOLD, 18));
        tituloLabel.setBounds(20, 20, 300, 30);
        add(tituloLabel);

        idLabel = new JLabel("ID");
        idLabel.setBounds(20, 70, 100, 30);
        add(idLabel);

        idTextField = new JTextField();
        idTextField.setBounds(130, 70, 150, 30);
        idTextField.setEditable(false);
        add(idTextField);

        JLabel nomeLabel = new JLabel("Nome:");
        nomeLabel.setBounds(20, 110, 100, 30);
        add(nomeLabel);

        nomeField = new JTextField();
        nomeField.setBounds(130, 110, 150, 30);
        add(nomeField);

        JLabel apelidoLabel = new JLabel("Apelido:");
        apelidoLabel.setBounds(20, 150, 100, 30);
        add(apelidoLabel);

        apelidoField = new JTextField();
        apelidoField.setBounds(130, 150, 150, 30);
        add(apelidoField);

        JLabel sexoLabel = new JLabel("Sexo:");
        sexoLabel.setBounds(20, 190, 100, 30); // Ajuste na posição vertical
        add(sexoLabel);

        sexoComboBox = new JComboBox<>(new String[]{"Masculino", "Feminino"});
        sexoComboBox.setBounds(130, 190, 150, 30); // Ajuste na posição vertical
        add(sexoComboBox);

        JLabel emailLabel = new JLabel("Email:");
        emailLabel.setBounds(320, 70, 100, 30);
        add(emailLabel);

        emailField = new JTextField();
        emailField.setBounds(420, 70, 150, 30);
        add(emailField);

        JLabel telefoneLabel = new JLabel("Telefone:");
        telefoneLabel.setBounds(320, 110, 100, 30);
        add(telefoneLabel);

        telefoneField = new JTextField();
        telefoneField.setBounds(420, 110, 150, 30);
        add(telefoneField);

        JLabel statusLabel = new JLabel("Status:");
        statusLabel.setBounds(600, 70, 100, 30);
        add(statusLabel);

        statusCheckBox = new JCheckBox("Activo");
        statusCheckBox.setSelected(true);
        statusCheckBox.setBounds(700, 70, 80, 30);
        add(statusCheckBox);

        JLabel isAdminLabel = new JLabel("isAdmin:");
        isAdminLabel.setBounds(600, 110, 100, 30);
        add(isAdminLabel);

        isAdminCheckBox = new JCheckBox("Sim");
        isAdminCheckBox.setBounds(700, 110, 80, 30);
        isAdminCheckBox.setEnabled(false);
        add(isAdminCheckBox);

        JLabel nomeUsuarioLabel = new JLabel("Nome de usuario:");
        nomeUsuarioLabel.setBounds(320, 150, 100, 30);
        add(nomeUsuarioLabel);

        nomeUsuarioField = new JTextField();
        nomeUsuarioField.setBounds(420, 150, 150, 30);
        add(nomeUsuarioField);

        JLabel passwordLabel = new JLabel("Senha:");
        passwordLabel.setBounds(320, 190, 100, 30);
        add(passwordLabel);

        passwordField = new JPasswordField();
        passwordField.setBounds(420, 190, 150, 30);
        add(passwordField);

        JLabel confirmarPasswordLabel = new JLabel("Confirmar Senha:");
        confirmarPasswordLabel.setBounds(320, 230, 100, 30);
        add(confirmarPasswordLabel);

        confirmarPasswordField = new JPasswordField();
        confirmarPasswordField.setBounds(420, 230, 150, 30);
        confirmarPasswordField.setEnabled(false);
        add(confirmarPasswordField);

        JLabel turmasLabel = new JLabel("Turmas:");
        turmasLabel.setBounds(600, 150, 100, 30);
        add(turmasLabel);

        turmasList = new JList<>();
        turmasList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        JScrollPane scrollPanes = new JScrollPane(turmasList);
        scrollPanes.setBounds(600, 170, 250, 120); // Ajuste: Maior altura para exibir múltiplas turmas
        add(scrollPanes);

        // Tabela de professores
        String[] colunas = {"ID", "Nome", "Apelido", "Sexo", "Email", "Telefone", "Usuario", "Status", "isAdmin"};
        tableModel = new DefaultTableModel(colunas, 0);
        professoresTable = new JTable(tableModel);
        JScrollPane scrollPane = new JScrollPane(professoresTable);
        scrollPane.setBounds(20, 330, 830, 160);
        add(scrollPane);

        // Botões
        salvarButton = new JButton("Salvar");
        salvarButton.setBounds(130, 280, 100, 30);
        add(salvarButton);

        actualizarButton = new JButton("Actualizar");
        actualizarButton.setBounds(260, 280, 100, 30);
        add(actualizarButton);

        eliminarButton = new JButton("Eliminar");
        eliminarButton.setBounds(390, 280, 100, 30);
        add(eliminarButton);

        limparButton = new JButton("Limpar");
        limparButton.setBounds(520, 280, 100, 30);
        add(limparButton);

        carregarDadosTurmas();
    }

    private void carregarDadosTurmas() {

        CrudDeTurmas crudDeTurmas = new CrudDeTurmas("turmas.dat");
        List<Turma> turmas = crudDeTurmas.readAll();

        DefaultListModel<Turma> model = new DefaultListModel<>();
        for (Turma turma : turmas) {
            model.addElement(turma);
        }
        turmasList.setModel(model);
        turmasList.setCellRenderer(new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                if (value instanceof Turma) {
                    Turma turma = (Turma) value;
                    setText(turma.getNome());
                }
                return this;
            }
        });
    }

    public List<Turma> getSelectedTurmas() {
        return turmasList.getSelectedValuesList();
    }

    // Métodos de acesso aos componentes
    public JTextField getIdTextField() {
        return idTextField;
    }

    public JTextField getNomeField() {
        return nomeField;
    }

    public JTextField getApelidoField() {
        return apelidoField;
    }

    public JComboBox<String> getSexoComboBox() {
        return sexoComboBox;
    }

    public JTextField getEmailField() {
        return emailField;
    }

    public JTextField getTelefoneField() {
        return telefoneField;
    }

    public JCheckBox getStatusCheckBox() {
        return statusCheckBox;
    }

    public JCheckBox getAdminCheckBox() {
        return isAdminCheckBox;
    }

    public JTextField getNomeUsuarioField() {
        return nomeUsuarioField;
    }

    public JPasswordField getPasswordField() {
        return passwordField;
    }

    public JPasswordField getConfirmarPasswordField() {
        return confirmarPasswordField;
    }

    public List<Turma> getTurmasSelecionadas() {
        return turmasList.getSelectedValuesList();
    }

    public JButton getSalvarButton() {
        return salvarButton;
    }

    public JButton getActualizarButton() {
        return actualizarButton;
    }

    public JButton getEliminarButton() {
        return eliminarButton;
    }

    public JButton getLimparButton() {
        return limparButton;
    }

    public JTable getProfessoresTable() {
        return professoresTable;
    }

    public DefaultTableModel getTableModel() {
        return tableModel;
    }

    // Métodos para adicionar listeners aos botões
    public void addSalvarButtonListener(ActionListener listener) {
        salvarButton.addActionListener(listener);
    }

    public void addActualizarButtonListener(ActionListener listener) {
        actualizarButton.addActionListener(listener);
    }

    public void addEliminarButtonListener(ActionListener listener) {
        eliminarButton.addActionListener(listener);
    }

    public void addLimparButtonListener(ActionListener listener) {
        limparButton.addActionListener(listener);
    }
}