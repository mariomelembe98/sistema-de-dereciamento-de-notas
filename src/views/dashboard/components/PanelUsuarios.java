package views.dashboard.components;

import controller.UsuarioController;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class PanelUsuarios extends JPanel {
    private JLabel lblUsuario;
    private JTextField idTextField;
    private JTextField nomeField;
    private JTextField nomeUsuarioField;
    private JTextField emailField;
    private JPasswordField senhaField;
    private JCheckBox adminCheckBox;
    private JCheckBox statusCheckBox;
    private JTextField pesquisaField;
    private JButton pesquisarButton;
    private JButton salvarButton;
    private JButton carregarButton;
    private JButton actualizarButton;
    private JButton eliminarButton;
    private JButton limparButton;
    private JTable usuariosTable;
    private DefaultTableModel tableModel;
    private int proximoId = 1;
    private UsuarioController usuarioController;

    public PanelUsuarios() {
        setBackground(Color.WHITE);
        setLayout(null);

        lblUsuario = new JLabel("Cadastro de Usuário");
        lblUsuario.setFont(new Font("Tahoma", Font.BOLD, 14));
        lblUsuario.setForeground(Color.BLACK);
        lblUsuario.setBounds(80,0,500,50);
        add(lblUsuario);

        // Campos de entrada
        JLabel idLabel = new JLabel("ID");
        idLabel.setBounds(80, 50, 80, 30);
        add(idLabel);

        idTextField = new JTextField();
        idTextField.setBounds(170, 50, 80, 30);
        idTextField.setEditable(false);
        add(idTextField);

        JLabel nomeLabel = new JLabel("Nome:");
        nomeLabel.setBounds(80, 90, 80, 30);
        add(nomeLabel);

        nomeField = new JTextField();
        nomeField.setBounds(170, 90, 200, 30);
        add(nomeField);

        JLabel nomeUsuarioLabel = new JLabel("Nome de Usuario:");
        nomeUsuarioLabel.setBounds(80, 130, 80, 30);
        add(nomeUsuarioLabel);

        nomeUsuarioField = new JTextField();
        nomeUsuarioField.setBounds(170, 130, 200, 30);
        add(nomeUsuarioField);

        JLabel emailLabel = new JLabel("Email:");
        emailLabel.setBounds(80, 170, 80, 30);
        add(emailLabel);

        emailField = new JTextField();
        emailField.setBounds(170, 170, 200, 30);
        add(emailField);

        JLabel senhaLabel = new JLabel("Senha:");
        senhaLabel.setBounds(80, 210, 80, 30);
        add(senhaLabel);

        senhaField = new JPasswordField();
        senhaField.setBounds(170, 210, 200, 30);
        add(senhaField);

        adminCheckBox = new JCheckBox("Admin");
        adminCheckBox.setBounds(400, 50, 80, 30);
        add(adminCheckBox);

        statusCheckBox = new JCheckBox("Status");
        statusCheckBox.setBounds(400, 90, 80, 30);
        add(statusCheckBox);

        JLabel pesquisaLabel = new JLabel("Pesquisa:");
        pesquisaLabel.setBounds(560, 50, 80, 30);
        add(pesquisaLabel);

        pesquisaField = new JTextField();
        pesquisaField.setBounds(630, 50, 150, 30);
        add(pesquisaField);

        // Botões
        salvarButton = new JButton("Salvar");
        salvarButton.setBackground(Color.GREEN);
        salvarButton.setForeground(Color.WHITE);
        salvarButton.setBounds(80, 270, 100, 30);
        add(salvarButton);

        carregarButton = new JButton("Carregar");
        carregarButton.setBounds(200, 270, 100, 30);
        //carregarButton.setEnabled(false);
        add(carregarButton);

        pesquisarButton = new JButton("Pesquisar");
        pesquisarButton.setBounds(680, 270, 100, 30);
        add(pesquisarButton);

        actualizarButton = new JButton("Actualizar");
        actualizarButton.setBounds(320, 270, 100, 30);
        add(actualizarButton);

        eliminarButton = new JButton("Eliminar");
        eliminarButton.setBackground(Color.RED);
        eliminarButton.setForeground(Color.WHITE);
        eliminarButton.setBounds(440, 270, 100, 30);
        add(eliminarButton);

        limparButton = new JButton("Limpar");
        limparButton.setBounds(560, 270, 100, 30);
        add(limparButton);

        // Configurando a tabela de usuários
        tableModel = new DefaultTableModel();
        tableModel.addColumn("ID");
        tableModel.addColumn("Nome");
        tableModel.addColumn("Nome Usuario");
        tableModel.addColumn("Email");
        tableModel.addColumn("Admin");
        tableModel.addColumn("Status");

        usuariosTable = new JTable(tableModel);
        JScrollPane scrollPane = new JScrollPane(usuariosTable);
        scrollPane.setBounds(80, 320, 700, 180);
        add(scrollPane);

        // Inicializando o próximo ID com base no número de linhas na tabela
        proximoId = tableModel.getRowCount() + 1;
        idTextField.setText(String.valueOf(proximoId));
    }

    // Métodos de acesso aos componentes
    public JTextField getIdTextField() {
        return idTextField;
    }

    public JTextField getNomeField() {
        return nomeField;
    }

    public JTextField getNomeUsuarioField() {
        return nomeUsuarioField;
    }

    public JTextField getEmailField() {
        return emailField;
    }

    public JPasswordField getSenhaField() {
        return senhaField;
    }

    public JCheckBox getStatusCheckBox() {
        return statusCheckBox;
    }

    public JCheckBox getAdminCheckBox() {
        return adminCheckBox;
    }

    public JButton getSalvarButton() {
        return salvarButton;
    }

    public JButton getCarregarButton() {
        return carregarButton;
    }

    public JButton getActualizarButton (){
        return actualizarButton;
    }

    public JButton getEliminarButton (){
        return eliminarButton;
    }

    public JButton getLimparButton() {
        return limparButton;
    }

    public JTable getUsuariosTable() {
        return usuariosTable;
    }

    public JTextField getPesquisaField() {
        return pesquisaField;
    }

    public JButton getPesquisarButton() {
        return pesquisarButton;
    }

}
