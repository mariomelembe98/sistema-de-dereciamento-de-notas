package views.dashboard.components;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionListener;

public class PanelCursos extends JPanel {
    private JTextField nomeField;
    private JTextField codigoField;
    private JTextArea descricaoField;
    private JButton salvarButton;
    private JButton actualizarButton;
    private JButton eliminarButton;
    private JButton limparButton;
    private JTable cursosTable;
    private DefaultTableModel tableModel;

    public PanelCursos() {
        setBackground(Color.WHITE);
        setLayout(null);

        // Campos de texto e rótulos
        JLabel tituloLabel = new JLabel("Cadastro de Cursos");
        tituloLabel.setFont(new Font("Tahoma", Font.BOLD, 18));
        tituloLabel.setBounds(20, 20, 300, 30);
        add(tituloLabel);


        JLabel codigoLabel = new JLabel("Código:");
        codigoLabel.setBounds(20, 70, 100, 30);
        add(codigoLabel);

        codigoField = new JTextField();
        codigoField.setBounds(130, 70, 200, 30);
        codigoField.setEnabled(false);
        add(codigoField);

        JLabel nomeLabel = new JLabel("Nome:");
        nomeLabel.setBounds(20, 110, 100, 30);
        add(nomeLabel);

        nomeField = new JTextField();
        nomeField.setBounds(130, 110, 200, 30);
        add(nomeField);

        JLabel duracaoLabel = new JLabel("Descrição:");
        duracaoLabel.setBounds(400, 70, 100, 30);
        add(duracaoLabel);

        descricaoField = new JTextArea();
        descricaoField.setBounds(460, 70, 200, 100);
        descricaoField.setBackground(Color.LIGHT_GRAY);
        add(descricaoField);

        // Tabela de cursos
        String[] colunas = {"ID", "Nome", "Descrição"};
        tableModel = new DefaultTableModel(colunas, 0);
        cursosTable = new JTable(tableModel);
        JScrollPane scrollPane = new JScrollPane(cursosTable);
        scrollPane.setBounds(20, 250, 780, 270);
        add(scrollPane);

        // Botões
        salvarButton = new JButton("Salvar");
        salvarButton.setBounds(20, 200, 100, 30);
        add(salvarButton);

        actualizarButton = new JButton("Actualizar");
        actualizarButton.setBounds(130, 200, 100, 30);
        add(actualizarButton);

        eliminarButton = new JButton("Remover");
        eliminarButton.setBounds(250, 200, 100, 30);
        add(eliminarButton);

        limparButton = new JButton("Limpar");
        limparButton.setBounds(370, 200, 100, 30);
        add(limparButton);

    }

    public JTextField codigoField() {
        return codigoField;
    }

    public JTextField getNomeField() {
        return nomeField;
    }

    public JTextField getCodigoField() {
        return codigoField;
    }

    public JTextArea getDescricaoField() {
        return descricaoField;
    }

    public JButton getSalvarButton() {
        return salvarButton;
    }

    public JButton getActualizarButton() {
        return actualizarButton;
    }

    public JButton getRemoverButton() {
        return eliminarButton;
    }

    public JButton getLimparButton() {
        return limparButton;
    }

    public JTable getCursosTable() {
        return cursosTable;
    }

    // Métodos para adicionar listeners aos botões
    public void addSalvarButtonListener(ActionListener listener) {
        salvarButton.addActionListener(listener);
    }

    public void addActualizarButtonListener(ActionListener listener) {
        actualizarButton.addActionListener(listener);
    }

    public void addEliminarButtonListener(ActionListener listener) {
        eliminarButton.addActionListener(listener);
    }

    public void addLimparButtonListener(ActionListener listener) {
        limparButton.addActionListener(listener);
    }

}
