package views.dashboard.components;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PanelHome extends JPanel {
    private JLabel tituloLabel;
    private JLabel descricaoLabel;
    private String tituloText = "Bem-vindo ao Sistema de Gestão de Notas";
    private String descricaoText = "Este é um sistema para gerenciamento de Notas.";
    private int tituloIndex = 0;
    private int descricaoIndex = 0;


    public PanelHome() {
        setLayout(null);
        tituloLabel = new JLabel();
        tituloLabel.setFont(new Font("Tahoma", Font.BOLD, 24));
        tituloLabel.setBounds(150, 100, 600, 50);
        add(tituloLabel);

        descricaoLabel = new JLabel();
        descricaoLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
        descricaoLabel.setBounds(200, 140, 500, 50);
        add(descricaoLabel);

        // Timer para o título
        Timer tituloTimer = new Timer(30, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (tituloIndex < tituloText.length()) {
                    tituloLabel.setText(tituloLabel.getText() + tituloText.charAt(tituloIndex));
                    tituloIndex++;
                } else {
                    ((Timer) e.getSource()).stop();
                    // Depois de terminar o título, começar a descrição
                    Timer descricaoTimer = new Timer(20, new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if (descricaoIndex < descricaoText.length()) {
                                descricaoLabel.setText(descricaoLabel.getText() + descricaoText.charAt(descricaoIndex));
                                descricaoIndex++;
                            } else {
                                ((Timer) e.getSource()).stop();
                            }
                        }
                    });
                    descricaoTimer.start();
                }
            }
        });
        tituloTimer.start();
    }

}
