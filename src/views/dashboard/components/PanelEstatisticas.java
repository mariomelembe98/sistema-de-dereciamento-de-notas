package views.dashboard.components;

import DAO.CrudDeProfessores;
import app.models.Professor;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.category.DefaultCategoryDataset;

import javax.swing.*;
import java.awt.*;
import java.util.List;

/**
 * Created by USER on 13/05/2024.
 */

public class PanelEstatisticas extends JPanel {

    private List<Professor> professores;

    public PanelEstatisticas() {
        setLayout(new BorderLayout());


        // Criar o conjunto de dados
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        CrudDeProfessores crudDeProfessores = new CrudDeProfessores("professores.dat");
        List<Professor> professores = crudDeProfessores.readAll();

        for (Professor professor : professores) {
            dataset.addValue(professor.getId(), "Número de XYZ", professor.getNome());
        }

        // Criar o gráfico de barras
        JFreeChart chart = ChartFactory.createBarChart(
                "Estatísticas dos Professores",
                "Professores",
                "Número de XYZ",
                dataset
        );

        // Adicionar o gráfico em um painel
        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setBounds(240, 10, 800, 600);
        add(chartPanel, BorderLayout.CENTER);

    }

}
