package views.dashboard.components;

import DAO.CrudDeEstudantes;
import DAO.CrudDeDisciplinas;
import DAO.CrudDeTurmas;
import app.models.Estudante;
import app.models.Disciplina;
import app.models.Turma;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PanelNotas extends JPanel {
    private JComboBox<String> cbTurma;
    private JComboBox<String> cbEstudante;
    private JComboBox<String> cbDisciplina;
    private JTextField txtNota1;
    private JTextField txtNota2;
    private JButton btnSalvar;
    private JButton btnAtualizar;
    private JButton btnEliminar;
    private JTable notasTable;
    private DefaultTableModel tableModel;

    private Map<String, Integer> turmaMap;
    private Map<String, Integer> estudanteMap;
    private Map<String, Integer> disciplinaMap;

    public PanelNotas() {
        setLayout(null);

        JLabel lblTurma = new JLabel("Turma:");
        lblTurma.setBounds(30, 30, 80, 30);
        add(lblTurma);

        cbTurma = new JComboBox<>();
        cbTurma.setBounds(120, 30, 150, 30);
        add(cbTurma);

        JLabel lblEstudante = new JLabel("Estudante:");
        lblEstudante.setBounds(30, 70, 80, 30);
        add(lblEstudante);

        cbEstudante = new JComboBox<>();
        cbEstudante.setBounds(120, 70, 150, 30);
        add(cbEstudante);

        JLabel lblDisciplina = new JLabel("Disciplina:");
        lblDisciplina.setBounds(30, 110, 80, 30);
        add(lblDisciplina);

        cbDisciplina = new JComboBox<>();
        cbDisciplina.setBounds(120, 110, 150, 30);
        add(cbDisciplina);

        JLabel lblNota1 = new JLabel("Nota 1:");
        lblNota1.setBounds(30, 150, 80, 30);
        add(lblNota1);

        txtNota1 = new JTextField();
        txtNota1.setBounds(120, 150, 150, 30);
        add(txtNota1);

        JLabel lblNota2 = new JLabel("Nota 2:");
        lblNota2.setBounds(30, 190, 80, 30);
        add(lblNota2);

        txtNota2 = new JTextField();
        txtNota2.setBounds(120, 190, 150, 30);
        add(txtNota2);

        btnSalvar = new JButton("Salvar");
        btnSalvar.setBounds(30, 230, 100, 30);
        add(btnSalvar);

        btnAtualizar = new JButton("Atualizar");
        btnAtualizar.setBounds(140, 230, 100, 30);
        add(btnAtualizar);

        btnEliminar = new JButton("Eliminar");
        btnEliminar.setBounds(250, 230, 100, 30);
        add(btnEliminar);

        tableModel = new DefaultTableModel();
        tableModel.addColumn("ID");
        tableModel.addColumn("Estudante");
        tableModel.addColumn("Disciplina");
        tableModel.addColumn("Nota 1");
        tableModel.addColumn("Nota 2");
        tableModel.addColumn("Media");

        notasTable = new JTable(tableModel);
        JScrollPane scrollPane = new JScrollPane(notasTable);
        scrollPane.setBounds(30, 280, 700, 200);
        add(scrollPane);

        // Inicializa os mapas
        turmaMap = new HashMap<>();
        estudanteMap = new HashMap<>();
        disciplinaMap = new HashMap<>();

        // Carrega os dados
        //carregarDadosTurmas();
        carregarDadosEstudantes();
        carregarDadosDisciplinas();
    }

    private void carregarDadosTurmas() {
        CrudDeTurmas crudDeTurmas = new CrudDeTurmas("turmas.dat");
        List<Turma> turmas = crudDeTurmas.readAll();
        for (Turma turma : turmas) {
            cbTurma.addItem(turma.getNome());
            turmaMap.put(turma.getNome(), turma.getId());
        }
    }

    private void carregarDadosEstudantes() {
        CrudDeEstudantes crudDeEstudantes = new CrudDeEstudantes("estudantes.dat");
        List<Estudante> estudantes = crudDeEstudantes.readAll();
        for (Estudante estudante : estudantes) {
            cbEstudante.addItem(estudante.getNome());
            estudanteMap.put(estudante.getNome(), estudante.getId());
        }
    }

    private void carregarDadosDisciplinas() {
        CrudDeDisciplinas crudDeDisciplinas = new CrudDeDisciplinas("disciplinas.dat");
        List<Disciplina> disciplinas = crudDeDisciplinas.readAll();
        for (Disciplina disciplina : disciplinas) {
            cbDisciplina.addItem(disciplina.getNome());
            disciplinaMap.put(disciplina.getNome(), disciplina.getId());
        }
    }

    // Getters para os componentes

    public JComboBox<String> getCbTurma() {
        return cbTurma;
    }

    public JComboBox<String> getCbEstudante() {
        return cbEstudante;
    }

    public JComboBox<String> getCbDisciplina() {
        return cbDisciplina;
    }

    public JTextField getTxtNota1() {
        return txtNota1;
    }

    public JTextField getTxtNota2() {
        return txtNota2;
    }

    public JButton getBtnSalvar() {
        return btnSalvar;
    }

    public JButton getBtnAtualizar() {
        return btnAtualizar;
    }

    public JButton getBtnEliminar() {
        return btnEliminar;
    }

    public JTable getNotasTable() {
        return notasTable;
    }

    public DefaultTableModel getTableModel() {
        return tableModel;
    }

    public int getIdTurmaSelecionada() {
        String turmaSelecionada = (String) cbTurma.getSelectedItem();
        return turmaMap.get(turmaSelecionada);
    }

    public int getIdEstudanteSelecionado() {
        String estudanteSelecionado = (String) cbEstudante.getSelectedItem();
        return estudanteMap.get(estudanteSelecionado);
    }

    public int getIdDisciplinaSelecionada() {
        String disciplinaSelecionada = (String) cbDisciplina.getSelectedItem();
        return disciplinaMap.get(disciplinaSelecionada);
    }
}
