package views.dashboard.components;

import com.formdev.flatlaf.FlatClientProperties;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionListener;

public class PanelEstudantes extends JPanel {
    private JLabel lblText;
    private JTextField idTextField;
    private JTextField codigoTextField;
    private JTextField nomeField;
    private JTextField sobrenomeField;
    private JTextField dataNascimentoField;
    private JComboBox<String> sexoComboBox;
    private JComboBox<String> turmaComboBox;
    private JTextField emailField;
    private JTextField telefoneField;
    private JComboBox turnoCombobox;
    private JComboBox nivelCombobox;
    private JComboBox<String> cursoComboBox;
    private JTextField nomeUsuarioField;
    private JPasswordField senhaField;
    private JCheckBox adminCheckBox;
    private JCheckBox statusCheckBox;
    private JButton salvarButton;
    private JButton actualizarButton;
    private JButton eliminarButton;
    private JButton limparButton;
    private JTable estudantesTable;
    private DefaultTableModel tableModel;

    public PanelEstudantes() {
        setBackground(Color.WHITE);
        setLayout(null);

        // Campos de entrada
        lblText = new JLabel("Matricular Estudantes");
        lblText.setFont(new Font("Tahoma", Font.BOLD, 20));
        lblText.setBounds(20, 15, 300, 20);
        add(lblText);

        JLabel lblId = new JLabel("ID");
        lblId.setBounds(20, 60, 150, 30);
        add(lblId);

        idTextField = new JTextField();
        idTextField.setEditable(false);
        idTextField.setBounds(120, 60, 150, 30);
        add(idTextField);

        JLabel codigoLabel = new JLabel("Código:");
        codigoLabel.setBounds(300, 60, 80, 30);
        add(codigoLabel);

        codigoTextField = new JTextField();
        codigoTextField.setBounds(360, 60, 150, 30);
        codigoTextField.setEnabled(false);
        add(codigoTextField);

        JLabel nomeLabel = new JLabel("Nome:");
        nomeLabel.setBounds(20, 100, 80, 30);
        add(nomeLabel);

        nomeField = new JTextField();
        nomeField.setBounds(120, 100, 150, 30);
        add(nomeField);

        JLabel sobrenomeLabel = new JLabel("Sobrenome:");
        sobrenomeLabel.setBounds(20, 140, 80, 30);
        add(sobrenomeLabel);

        sobrenomeField = new JTextField();
        sobrenomeField.setBounds(120, 140, 150, 30);
        add(sobrenomeField);


        JLabel dataNascimentoLabel = new JLabel("Data de Nascimento:");
        dataNascimentoLabel.setBounds(20, 180, 90, 30);
        add(dataNascimentoLabel);

        dataNascimentoField = new JTextField();
        dataNascimentoField.setBounds(120, 180, 150, 30);
        dataNascimentoField.putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, "DD/MM/AAAA");
        add(dataNascimentoField);

        JLabel sexoLabel = new JLabel("Sexo:");
        sexoLabel.setBounds(20, 220, 80, 30);
        add(sexoLabel);

        sexoComboBox = new JComboBox<>(new String[]{"Masculino", "Feminino"});
        sexoComboBox.setBounds(120, 220, 150, 30);
        add(sexoComboBox);

        JLabel emailLabel = new JLabel("Email:");
        emailLabel.setBounds(300, 100, 80, 30);
        add(emailLabel);

        emailField = new JTextField();
        emailField.setBounds(360, 100, 150, 30);
        add(emailField);

        JLabel telefoneLabel = new JLabel("Telefone:");
        telefoneLabel.setBounds(300, 140, 80, 30);
        add(telefoneLabel);

        telefoneField = new JTextField();
        telefoneField.setBounds(360, 140, 150, 30);
        add(telefoneField);

        JLabel enderecoLabel = new JLabel("Turno:");
        enderecoLabel.setBounds(300, 180, 80, 30);
        add(enderecoLabel);

        turnoCombobox = new JComboBox<>(new String[]{"Manha", "Tarde", "Noite"});
        turnoCombobox.setBounds(360, 180, 150, 30);
        add(turnoCombobox);

        JLabel nivelLabel = new JLabel("Nivel:");
        nivelLabel.setBounds(550, 220, 80, 30);
        add(nivelLabel);

        nivelCombobox = new JComboBox<>(new String[]{"Basico", "Media", "Avancado"});
        nivelCombobox.setBounds(650, 220, 150, 30);
        nivelCombobox.setEnabled(false);
        add(nivelCombobox);

        JLabel cursoLabel = new JLabel("Curso:");
        cursoLabel.setBounds(300, 220, 80, 30);
        add(cursoLabel);

        cursoComboBox = new JComboBox<>(new String[]{"Engenharia", "Ciência da Computação", "Matemática", "Artes"});
        cursoComboBox.setBounds(360, 220, 150, 30);
        add(cursoComboBox);

        JLabel nomeUsuarioLabel = new JLabel("Nome de Usuário:");
        nomeUsuarioLabel.setBounds(550, 60, 150, 30);
        add(nomeUsuarioLabel);

        nomeUsuarioField = new JTextField();
        nomeUsuarioField.setBounds(650, 60, 150, 30);
        add(nomeUsuarioField);

        JLabel senhaLabel = new JLabel("Senha:");
        senhaLabel.setBounds(550, 100, 80, 30);
        add(senhaLabel);

        senhaField = new JPasswordField();
        senhaField.setBounds(650, 100, 150, 30);
        add(senhaField);

        adminCheckBox = new JCheckBox("Administrador");
        adminCheckBox.setBounds(550, 140, 80, 30);
        adminCheckBox.setSelected(false);
        adminCheckBox.setEnabled(false);
        add(adminCheckBox);


        statusCheckBox = new JCheckBox("Activo");
        statusCheckBox.setBounds(650, 140, 80, 30);
        statusCheckBox.setSelected(true);
        add(statusCheckBox);

        JLabel turmaLabel = new JLabel("Turma");
        turmaLabel.setBounds(550, 180,150, 30);
        add(turmaLabel);

        turmaComboBox = new JComboBox<>(new String[]{"Turma A", "Turma B"});
        turmaComboBox.setBounds(650,180,150,30);
        add(turmaComboBox);

        // Botões
        salvarButton = new JButton("Salvar");
        salvarButton.setBounds(20, 300, 100, 30);
        add(salvarButton);

        actualizarButton = new JButton("Actualizar");
        actualizarButton.setBounds(140, 300, 100, 30);
        add(actualizarButton);

        eliminarButton = new JButton("Eliminar");
        eliminarButton.setBounds(260, 300, 100, 30);
        add(eliminarButton);

        limparButton = new JButton("Limpar");
        limparButton.setBounds(380, 300, 100, 30);
        add(limparButton);

        // Configurando a tabela de estudantes
        tableModel = new DefaultTableModel();
        tableModel.addColumn("ID");
        tableModel.addColumn("Código");
        tableModel.addColumn("Nome");
        tableModel.addColumn("Sobrenome");
        tableModel.addColumn("Data de Nascimento");
        tableModel.addColumn("Sexo");
        tableModel.addColumn("Email");
        tableModel.addColumn("Telefone");
        tableModel.addColumn("Turno");
        tableModel.addColumn("Nivel");
        tableModel.addColumn("Curso");
        tableModel.addColumn("N. Usuario");
        //tableModel.addColumn("Senha");
        tableModel.addColumn("Status");

        estudantesTable = new JTable(tableModel);
        JScrollPane scrollPane = new JScrollPane(estudantesTable);
        scrollPane.setBounds(20, 340, 820, 170);
        add(scrollPane);
    }

    public JTextField getIdTextField() {
        return idTextField;
    }

    public JTextField getCodigoTextField() {
        return codigoTextField;
    }

    public JTextField getNomeTextField() {
        return nomeField;
    }

    public JTextField getSobrenomeTextField() {
        return sobrenomeField;
    }

    public JTextField getDataNascimentoTextField() {
        return dataNascimentoField;
    }

    public JComboBox<String> getSexoComboBox() {
        return sexoComboBox;
    }

    public JTextField getEmailTextField() {
        return emailField;
    }

    public JTextField getTelefoneTextField() {
        return telefoneField;
    }

    public JComboBox<String>  getTurnoCombobox() {
        return turnoCombobox;
    }

    public JComboBox<String> getNivelCombobox(){
        return nivelCombobox;
    }

    public JComboBox<String> getCursoComboBox() {
        return cursoComboBox;
    }

    public JTextField getNomeUsuarioTextField() {
        return nomeUsuarioField;
    }

    public JPasswordField getSenhaTextField() {
        return senhaField;
    }

    public JCheckBox getAdminCheckBox(){
        return adminCheckBox;
    }

    public JCheckBox getStatusCheckBox() {
        return statusCheckBox;
    }

    public JComboBox getTurmaCombobox(){
        return turmaComboBox;
    }

    public JTable getEstudantesTable() {
        return estudantesTable;
    }

    public JButton getSalvarButton() {
        return salvarButton;
    }

    public JButton getLimparButton() {
        return limparButton;
    }

    public JButton getActualizarButton() {
        return actualizarButton;
    }

    public JButton getEliminarButton() {
        return eliminarButton;
    }

    // Métodos para adicionar listeners aos botões
    public void addSalvarButtonListener(ActionListener listener) {
        salvarButton.addActionListener(listener);
    }

    public void addActualizarButtonListener(ActionListener listener) {
        actualizarButton.addActionListener(listener);
    }

    public void addEliminarButtonListener(ActionListener listener) {
        eliminarButton.addActionListener(listener);
    }

    public void addLimparButtonListener(ActionListener listener) {
        limparButton.addActionListener(listener);
    }
}
