package views.dashboard.components;

import DAO.CrudDeProfessores;
import app.models.Professor;
import app.models.Turma;
import com.formdev.flatlaf.FlatClientProperties;
import controller.TurmaController;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class PanelTurmas extends JPanel {
    private JLabel lblTurma;
    private DefaultTableModel tableModel;
    private JTable turmasTable;
    private JTextField nomeField;
    private JComboBox<String> anoComboBox;
    private JComboBox<String> semestreComboBox;
    private JComboBox<String> turnoComboBox;
    private JList<Professor> professorList;
    private JButton salvarButton;
    private JButton atualizarButton;
    private JButton excluirButton;
    private JButton limparButton;
    private int turmaSelecionadaId = -1;
    private TurmaController controller;

    public PanelTurmas() {
        setLayout(null);

        lblTurma = new JLabel("Registar Turmas");
        lblTurma.setFont(new Font("Tahoma", Font.BOLD, 20));
        lblTurma.setBounds(20, 15, 300, 20);
        add(lblTurma);

        // Campos de entrada
        JLabel nomeLabel = new JLabel("Nome da Turma:");
        nomeLabel.setBounds(30, 60, 100, 30);
        add(nomeLabel);

        nomeField = new JTextField();
        nomeField.setBounds(140, 60, 200, 30);
        nomeField.putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, "Nome da turma");
        add(nomeField);

        JLabel anoLabel = new JLabel("Ano:");
        anoLabel.setBounds(30, 100, 100, 30);
        add(anoLabel);

        anoComboBox = new JComboBox<>(new String[]{"1º ano", "2º ano", "3º ano", "4º ano"}); // Exemplo de anos
        anoComboBox.setBounds(140, 100, 200, 30);
        add(anoComboBox);

        JLabel semestreLabel = new JLabel("Semestre:");
        semestreLabel.setBounds(30, 140, 100, 30);
        add(semestreLabel);

        semestreComboBox = new JComboBox<>(new String[]{"1º Semestre", "2º Semestre"}); // Exemplo de semestres
        semestreComboBox.setBounds(140, 140, 200, 30);
        add(semestreComboBox);

        JLabel turnoLabel = new JLabel("Turno:");
        turnoLabel.setBounds(370, 60, 100, 30);
        add(turnoLabel);

        turnoComboBox = new JComboBox<>(new String[]{"Manhã", "Tarde", "Noite"});
        turnoComboBox.setBounds(430, 60, 150, 30);
        add(turnoComboBox);

        JLabel professorLabel = new JLabel("Professor:");
        professorLabel.setBounds(370, 100, 100, 30);
        add(professorLabel);

        professorList = new JList<>();
        professorList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        JScrollPane scrollPane = new JScrollPane(professorList);
        scrollPane.setBounds(430, 100, 150, 80);
        add(scrollPane);

        // Botão de salvar
        salvarButton = new JButton("Salvar");
        salvarButton.setBounds(140, 180, 100, 30);
        salvarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                salvarTurma();
            }
        });
        add(salvarButton);

        // Botão de atualizar
        atualizarButton = new JButton("Atualizar");
        atualizarButton.setBounds(250, 180, 100, 30);
        atualizarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                atualizarTurma();
            }
        });
        add(atualizarButton);

        // Botão de excluir
        excluirButton = new JButton("Excluir");
        excluirButton.setBounds(360, 180, 100, 30);
        excluirButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                excluirTurma();
            }
        });
        add(excluirButton);

        limparButton = new JButton("Limpar");
        limparButton.setBounds(470, 180, 100, 30);
        limparButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                limparCampos();
            }
        });
        add(limparButton);

        // Configurando a tabela de turmas
        tableModel = new DefaultTableModel();
        tableModel.addColumn("ID");
        tableModel.addColumn("Nome");
        tableModel.addColumn("Ano");
        tableModel.addColumn("Semestre");
        tableModel.addColumn("Turno");
        tableModel.addColumn("Professores");

        turmasTable = new JTable(tableModel);
        JScrollPane scrollPaneTable = new JScrollPane(turmasTable);
        scrollPaneTable.setBounds(30, 230, 700, 200);
        add(scrollPaneTable);

        // Listener para seleção de turma na tabela
        turmasTable.getSelectionModel().addListSelectionListener(event -> {
            int selectedRow = turmasTable.getSelectedRow();
            if (selectedRow != -1) {
                turmaSelecionadaId = (int) tableModel.getValueAt(selectedRow, 0);
                nomeField.setText((String) tableModel.getValueAt(selectedRow, 1));
                anoComboBox.setSelectedItem(tableModel.getValueAt(selectedRow, 2));
                semestreComboBox.setSelectedItem(tableModel.getValueAt(selectedRow, 3));
                turnoComboBox.setSelectedItem(tableModel.getValueAt(selectedRow, 4));
                // Deselecionar professores
                professorList.clearSelection();
            }
        });

        carregarDadosProfessor();
    }

    private void salvarTurma() {
        String nome = nomeField.getText();
        String ano = (String) anoComboBox.getSelectedItem();
        String semestre = (String) semestreComboBox.getSelectedItem();
        String turno = (String) turnoComboBox.getSelectedItem();
        List<Professor> selectedProfessores = professorList.getSelectedValuesList();
        controller.salvarTurma(nome, ano, semestre, turno, selectedProfessores);
    }

    private void atualizarTurma() {
        if (turmaSelecionadaId != -1) {
            String nome = nomeField.getText();
            String ano = (String) anoComboBox.getSelectedItem();
            String semestre = (String) semestreComboBox.getSelectedItem();
            String turno = (String) turnoComboBox.getSelectedItem();
            List<Professor> selectedProfessores = professorList.getSelectedValuesList();
            controller.atualizarTurma(turmaSelecionadaId, nome, ano, semestre, turno, selectedProfessores);
        } else {
            JOptionPane.showMessageDialog(null, "Selecione uma turma para atualizar.");
        }
    }

    private void excluirTurma() {
        if (turmaSelecionadaId != -1) {
            controller.excluirTurma(turmaSelecionadaId);
        } else {
            JOptionPane.showMessageDialog(null, "Selecione uma turma para excluir.");
        }
    }

    public void limparCampos() {
        nomeField.setText("");
        turmaSelecionadaId = -1;
        anoComboBox.setSelectedIndex(0);
        semestreComboBox.setSelectedIndex(0);
        turnoComboBox.setSelectedIndex(0);
        professorList.clearSelection();
    }

    private void carregarDadosProfessor() {
        CrudDeProfessores crudDeProfessores = new CrudDeProfessores("professores.dat");
        List<Professor> professores = crudDeProfessores.readAll();

        DefaultListModel<Professor> model = new DefaultListModel<>();
        for (Professor professor : professores) {
            model.addElement(professor);
        }
        professorList.setModel(model);
        professorList.setCellRenderer(new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                if (value instanceof Professor) {
                    Professor professor = (Professor) value;
                    setText(professor.getNome());
                }
                return this;
            }
        });
    }

    public void setController(TurmaController controller) {
        this.controller = controller;
    }

    public void atualizarTabela(List<Turma> turmas) {
        tableModel.setRowCount(0);

        for (Turma turma : turmas) {
            tableModel.addRow(new Object[]{
                    turma.getId(),
                    turma.getNome(),
                    turma.getAno(),
                    turma.getSemestre(),
                    turma.getTurno(),
                    turma.getProfessorIds()
            });
        }
    }
}
