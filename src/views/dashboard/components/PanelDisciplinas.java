package views.dashboard.components;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class PanelDisciplinas extends JPanel {
    private JLabel lblText;
    private JTextField idField;
    private JTextField nomeField;
    private JComboBox cursoCombobox;
    private JTable disciplinasTable;
    private JButton salvarButton;
    private JButton limparButton;
    private JButton carregarButton;
    private JButton atualizarButton;
    private JButton eliminarButton;

    public PanelDisciplinas() {
        setLayout(null);

        lblText = new JLabel("Registar Disciplinas");
        lblText.setFont(new Font("Tahoma", Font.BOLD, 20));
        lblText.setBounds(20, 15, 300, 20);
        add(lblText);

        JLabel idLabel = new JLabel("ID");
        idLabel.setBounds(20, 70, 100, 30);
        add(idLabel);

        idField = new JTextField();
        idField.setBounds(120, 70, 150, 30);
        idField.setEnabled(false);
        add(idField);

        JLabel nomeLabel = new JLabel("Nome da Disciplina:");
        nomeLabel.setBounds(20, 110, 100, 30);
        add(nomeLabel);

        nomeField = new JTextField();
        nomeField.setBounds(120, 110, 200, 30);
        add(nomeField);

        JLabel descricaoLabel = new JLabel("Curso:");
        descricaoLabel.setBounds(20, 150, 100, 30);
        add(descricaoLabel);

        cursoCombobox = new JComboBox<>(new String[]{"Informatica", "Direito", "Mecanica"});
        cursoCombobox.setBounds(120, 150, 200, 30);
        add(cursoCombobox);

        salvarButton = new JButton("Salvar");
        salvarButton.setBounds(20, 190, 100, 30);
        add(salvarButton);

        limparButton = new JButton("Limpar");
        limparButton.setBounds(130, 190, 100, 30);
        add(limparButton);

        carregarButton = new JButton("Carregar");
        carregarButton.setBounds(240, 190, 100, 30);
        carregarButton.setEnabled(false);
        add(carregarButton);

        atualizarButton = new JButton("Actualizar");
        atualizarButton.setBounds(350, 190, 100, 30);
        add(atualizarButton);

        eliminarButton = new JButton("Eliminar");
        eliminarButton.setBounds(460, 190, 100, 30);
        add(eliminarButton);

        disciplinasTable = new JTable(new DefaultTableModel(new Object[]{"ID", "Nome", "Curso"}, 0));
        JScrollPane scrollPane = new JScrollPane(disciplinasTable);
        scrollPane.setBounds(20, 230, 540, 250);
        add(scrollPane);
    }

    public JTextField getNomeField() {
        return nomeField;
    }

    public JComboBox getCursoCombobox() {
        return cursoCombobox;
    }

    public JTable getDisciplinasTable() {
        return disciplinasTable;
    }

    public JButton getSalvarButton() {
        return salvarButton;
    }

    public JButton getLimparButton() {
        return limparButton;
    }

    public JButton getCarregarButton() {
        return carregarButton;
    }

    public JButton getAtualizarButton() {
        return atualizarButton;
    }

    public JButton getEliminarButton() {
        return eliminarButton;
    }
}
