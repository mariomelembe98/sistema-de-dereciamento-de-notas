package views.dashboard;

import DAO.*;
import controller.*;
import com.formdev.flatlaf.FlatIntelliJLaf;
import views.dashboard.components.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TelaPrincipal {
    private JFrame frame;
    private JPanel panelContainer;
    private JButton sairButton;
    private JPanel headerPanel;
    private JPanel contentPanel;
    private Font fontButtons;
    private Font fontButton;
    private String nomeUsuario;

    public TelaPrincipal() {
        frame = new JFrame("Sistema Escolar");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(1100, 600);
        frame.setLayout(null);
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);

        headerPanel = new JPanel();
        headerPanel.setBounds(220, 0, 880, 30);
        headerPanel.setLayout(null);
        headerPanel.setBackground(Color.DARK_GRAY);
        frame.add(headerPanel);

        JLabel avatarLabel = new JLabel(new ImageIcon("path/to/avatar.png")); // Coloque o caminho correto para a imagem do avatar
        avatarLabel.setBounds(820, 5, 40, 40);
        headerPanel.add(avatarLabel);

        JLabel nomeUsuarioLabel = new JLabel(nomeUsuario);
        nomeUsuarioLabel.setForeground(Color.WHITE);
        nomeUsuarioLabel.setBounds(870, 15, 100, 20);
        headerPanel.add(nomeUsuarioLabel);

        JPanel panelEsquerdo = new JPanel(null);
        panelEsquerdo.setBackground(Color.DARK_GRAY);
        panelEsquerdo.setBounds(0, 0, 220, 570);
        frame.add(panelEsquerdo);

        JButton homeButton = new JButton("Home");
        fontButton = new Font("Tahoma", Font.BOLD, 24);
        homeButton.setFont(fontButton);
        homeButton.setForeground(Color.WHITE);
        homeButton.setBackground(Color.DARK_GRAY);
        homeButton.setBounds(0, 0, 220, 50);
        homeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CardLayout cardLayout = (CardLayout) panelContainer.getLayout();
                cardLayout.show(panelContainer, "home");
            }
        });
        panelEsquerdo.add(homeButton);

        fontButtons = new Font("Tahoma", Font.BOLD, 14);

        adicionarBotao(panelEsquerdo, "Estudantes", 100, "estudantes");
        adicionarBotao(panelEsquerdo, "Professores", 150, "professores");
        adicionarBotao(panelEsquerdo, "Disciplinas", 200, "disciplinas");
        adicionarBotao(panelEsquerdo, "Cursos", 250, "cursos");
        adicionarBotao(panelEsquerdo, "Avaliações", 300, "avaliacoes");
        adicionarBotao(panelEsquerdo, "Estatísticas", 350, "estatisticas");
        adicionarBotao(panelEsquerdo, "Turmas", 400, "turmas");
        adicionarBotao(panelEsquerdo, "Usuários", 450, "usuarios");

        sairButton = new JButton("Sair");
        sairButton.setBounds(20, 500, 180, 30);
        sairButton.setBackground(Color.RED);
        sairButton.setForeground(Color.WHITE);
        sairButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int confirmacao = JOptionPane.showConfirmDialog(null, "Deseja realmente sair?", "Sair", JOptionPane.YES_NO_OPTION);
                if (confirmacao == JOptionPane.YES_OPTION) {
                    System.exit(0);
                }
            }
        });
        panelEsquerdo.add(sairButton);

        panelContainer = new JPanel(new CardLayout());
        panelContainer.setBounds(220, 30, 880, 570);
        frame.add(panelContainer);

        adicionarPainel(new CrudDeUsuarios("usuarios.dat"), new PanelUsuarios(), "usuarios", UsuarioController.class);
        adicionarPainel(new CrudDeDisciplinas("disciplinas.dat"), new PanelDisciplinas(), "disciplinas", DisciplinaController.class);
        adicionarPainel(new CrudDeProfessores("professores.dat"), new PanelProfessores(), "professores", ProfessorController.class);
        adicionarPainel(new CrudDeEstudantes("estudantes.dat"), new PanelEstudantes(), "estudantes", EstudanteController.class);
        adicionarPainel(new CrudDeCursos("cursos.dat"), new PanelCursos(), "cursos", CursoController.class);
        adicionarPainel(new CrudDeNotas("notas.dat"), new PanelNotas(), "avaliacoes", NotaController.class);
        adicionarPainel(new CrudDeTurmas("turmas.dat"), new PanelTurmas(), "turmas", TurmaController.class);

        panelContainer.add(new PanelHome(), "home");
        panelContainer.add(new PanelEstatisticas(), "estatisticas");

        CardLayout cardLayout = (CardLayout) panelContainer.getLayout();
        cardLayout.show(panelContainer, "home");
    }

    private void adicionarBotao(JPanel panelEsquerdo, String nomeBotao, int posY, String nomePainel) {
        JButton button = new JButton(nomeBotao);
        button.setBounds(10, posY, 200, 40);
        button.setForeground(Color.WHITE);
        button.setBackground(Color.DARK_GRAY);
        button.setFont(fontButtons);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CardLayout cardLayout = (CardLayout) panelContainer.getLayout();
                cardLayout.show(panelContainer, nomePainel);
            }
        });
        panelEsquerdo.add(button);
    }

    private <T> void adicionarPainel(Object crudInstance, JPanel panel, String nomePainel, Class<T> controllerClass) {
        panelContainer.add(panel, nomePainel);
        try {
            if (controllerClass != null) {
                T controller = controllerClass.getConstructor(panel.getClass(), crudInstance.getClass()).newInstance(panel, crudInstance);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void exibir() {
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        FlatIntelliJLaf.setup();
        TelaPrincipal app = new TelaPrincipal();
        app.exibir();
    }
}
