package views.professor.components;

import DAO.CrudDeTurmas;
import app.models.Estudante;
import app.models.Professor;
import app.models.Turma;
import views.avaliacao.PanelAvaliacao;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class PanelProfessor extends JPanel {
    private Professor professor;

    public PanelProfessor(Professor professor) {
        this.professor = professor;
        initComponents();
    }

    private void initComponents() {
        setLayout(new BorderLayout());

        JLabel nameLabel = new JLabel("Professor: " + professor.getNome());
        nameLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
        add(nameLabel, BorderLayout.NORTH);

        JPanel turmasPanel = new JPanel(new GridLayout(0, 1));
        JScrollPane scrollPane = new JScrollPane(turmasPanel);
        add(scrollPane, BorderLayout.CENTER);

        List<Integer> turmaIds = professor.getTurmaIds();
        if (turmaIds.isEmpty()) {
            JLabel noTurmasLabel = new JLabel("Nenhuma turma associada a este professor.");
            turmasPanel.add(noTurmasLabel);
        } else {
            for (Integer turmaId : turmaIds) {
                Turma turma = obterTurmaPorId(turmaId);
                if (turma != null) {
                    JButton turmaButton = new JButton("Turma: " + turma.getNome());
                    turmaButton.setFont(new Font("Tahoma", Font.BOLD, 16));
                    turmaButton.addActionListener(e -> exibirAvaliacaoPanel(turma));
                    turmasPanel.add(turmaButton);
                }
            }
        }
    }

    private Turma obterTurmaPorId(int turmaId) {
        if (turmaId <= 0) {
            return null;
        }

        if (professor.getTurmaIds().contains(turmaId)) {
            try {
                CrudDeTurmas crudDeTurmas = new CrudDeTurmas("turmas.dat");
                return crudDeTurmas.read(turmaId);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }

    private void exibirAvaliacaoPanel(Turma turma) {
        List<Estudante> estudantes = obterEstudantesDaTurma(turma.getId());
        PanelAvaliacao panelAvaliacao = new PanelAvaliacao(turma, estudantes);
        JFrame frame = (JFrame) SwingUtilities.getWindowAncestor(this);
        frame.setContentPane(panelAvaliacao);
        frame.revalidate();
    }

    private List<Estudante> obterEstudantesDaTurma(int turmaId) {
        // Implemente a lógica para buscar estudantes da turma com base no ID
        // Retorne uma lista de estudantes
        return List.of(
                new Estudante(1, "Estudante 1"),
                new Estudante(2, "Estudante 2"),
                new Estudante(3, "Estudante 3")
        ); // Exemplo estático; você deve buscar os dados reais
    }
}
