// TelaProfessor.java
package views.professor;

import app.models.Professor;
import views.professor.components.PanelProfessor;

import javax.swing.*;
import java.awt.*;

public class TelaProfessor extends JFrame {
    private Professor professor;

    public TelaProfessor(Professor professor) {
        this.professor = professor;
        setTitle("Área do Professor");
        setSize(900, 500);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setResizable(false);

        JLabel label = new JLabel("Bem-vindo, " + professor.getNome() + "!", SwingConstants.CENTER);
        label.setFont(new Font("Arial", Font.BOLD, 24));
        label.setBounds(0, 20, 900, 50);
        add(label);

        PanelProfessor panel = new PanelProfessor(professor);
        panel.setBounds(0, 80, 900, 380);
        add(panel);

        // Adicione mais componentes e funcionalidades conforme necessário
    }

    public void exibir() {
        setVisible(true);
    }
}
