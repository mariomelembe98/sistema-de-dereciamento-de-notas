package views;

import app.models.Estudante;
import app.models.Usuario;
import app.services.LoginService;
import DAO.LoginListener;
import app.models.Professor;
import com.formdev.flatlaf.FlatClientProperties;
import raven.toast.Notifications;
import views.estudante.TelaEstudante;
import views.professor.TelaProfessor;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TelaLogin extends JFrame {
    private JTextField campoUsuario;
    private JPasswordField campoSenha;
    private JPasswordField campoConfirmarSenha;
    private JButton botaoLogin;
    private LoginService loginService;
    private LoginListener loginListener;

    public TelaLogin(LoginService loginService) {
        this.loginService = loginService;

        setTitle("Autenticação");
        setSize(400, 500);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setLayout(null);
        setResizable(false);
        Notifications.getInstance().setJFrame(this);

        JPanel panelColor = new JPanel();
        panelColor.setBackground(Color.ORANGE);
        panelColor.setBounds(0, 0, 400, 3);
        add(panelColor);

        JLabel autenticacaoText = new JLabel("Login");
        autenticacaoText.setForeground(Color.LIGHT_GRAY);
        autenticacaoText.setBounds(170, 100, 100, 50);
        autenticacaoText.setFont(new Font("Tahoma", Font.PLAIN, 26));
        add(autenticacaoText);

        JLabel labelUsuario = new JLabel("Usuário:");
        labelUsuario.setBounds(100, 195, 80, 30);
        add(labelUsuario);

        campoUsuario = new JTextField();
        campoUsuario.setBounds(100, 220, 200, 30);
        campoUsuario.putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, "Nome de Usuário");
        add(campoUsuario);

        JLabel labelSenha = new JLabel("Senha:");
        labelSenha.setBounds(100, 255, 80, 30);
        add(labelSenha);

        campoSenha = new JPasswordField();
        campoSenha.setBounds(100, 280, 200, 30);
        campoSenha.putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, "Senha");
        add(campoSenha);

        botaoLogin = new JButton("Login");
        botaoLogin.setBounds(100, 340, 200, 30);
        botaoLogin.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                realizarLogin();
            }
        });
        add(botaoLogin);
    }

    public void setLoginListener(LoginListener listener) {
        this.loginListener = listener;
    }

    private void realizarLogin() {
        String usuario = campoUsuario.getText();
        String senha = new String(campoSenha.getPassword());

        if (usuario == null || usuario.isEmpty() || senha.isEmpty()) {
            Notifications.getInstance().show(Notifications.Type.WARNING, Notifications.Location.TOP_CENTER,"Preencha todos os campos");
            return;
        }

        Object usuarioAutenticado = loginService.verificarCredenciais(usuario, senha);
        if (usuarioAutenticado != null) {
            if (usuarioAutenticado instanceof Professor) {
                Professor professor = (Professor) usuarioAutenticado;

                if (!professor.getStatus() && !professor.isAdmin()) {
                    Notifications.getInstance().show(Notifications.Type.WARNING, Notifications.Location.TOP_CENTER,"Seu status de professor está inativo. Entre em contacto com o administrador.!");
                    return;
                }

                TelaProfessor telaProfessor = new TelaProfessor(professor);
                telaProfessor.exibir();

            } else if (usuarioAutenticado instanceof Estudante) {

                Estudante estudante = (Estudante) usuarioAutenticado;

                if (!estudante.getStatus() && !estudante.isAdmin()) {
                    Notifications.getInstance().show(Notifications.Type.WARNING, Notifications.Location.TOP_CENTER,"A sua matrícula está temporariamente inativo. Entre em contacto com a direcção da sua faculdade.!");
                    return;
                }

                TelaEstudante telaEstudante = new TelaEstudante(estudante);
                telaEstudante.exibir();
            } else {

                Usuario usuario1 = (Usuario) usuarioAutenticado;

                if (!usuario1.isAdmin()){
                    TelaLogin telaLogin = new TelaLogin(loginService);
                    telaLogin.exibir();
                    Notifications.getInstance().show(Notifications.Type.WARNING, Notifications.Location.TOP_CENTER, "Login invalido!");

                }else {
                    if (loginListener != null) {
                        loginListener.onLoginSuccess();
                    }

                    Notifications.getInstance().show(Notifications.Type.SUCCESS, Notifications.Location.TOP_CENTER, "Login bem-sucedido!");
                }

            }
            dispose();
        } else {
            if (loginListener != null) {
                loginListener.onLoginFailed();
            }
        }
    }

    public void exibir() {
        setVisible(true);
    }
}
