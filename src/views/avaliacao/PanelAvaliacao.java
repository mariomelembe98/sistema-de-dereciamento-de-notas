package views.avaliacao;

import DAO.CrudDeEstudantes;
import DAO.CrudDeNotas;
import app.models.Estudante;
import app.models.Nota;
import app.models.Turma;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PanelAvaliacao extends JPanel {
    private Turma turma;
    private List<Estudante> estudantes;
    private Map<Estudante, Nota> notasMap = new HashMap<>();
    private JTable tabela;
    private JTextField nota1Field;
    private JTextField nota2Field;
    private JLabel nota1Label;
    private JLabel nota2Label;
    private Estudante estudanteSelecionado;
    private CrudDeNotas crudDeNotas;

    public PanelAvaliacao(Turma turma, List<Estudante> estudantes) {
        this.turma = turma;
        this.estudantes = estudantes;
        this.crudDeNotas = new CrudDeNotas("notas.dat");
        initComponents();
    }

    private void initComponents() {
        setLayout(null);

        JLabel titleLabel = new JLabel("Avaliação da Turma: " + turma.getNome());
        titleLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
        titleLabel.setBounds(10, 10, 300, 30);
        add(titleLabel);

        String[] colunas = {"Estudante", "Nota 1", "Nota 2", "Média"};
        DefaultTableModel tableModel = new DefaultTableModel(colunas, 0);
        tabela = new JTable(tableModel);
        JScrollPane scrollPane = new JScrollPane(tabela);
        scrollPane.setBounds(10, 50, 400, 200);
        add(scrollPane);

        // Labels and TextFields for "Nota 1" and "Nota 2"
        nota1Label = new JLabel("Nota 1:");
        nota1Label.setBounds(420, 50, 50, 25);
        add(nota1Label);

        nota1Field = new JTextField();
        nota1Field.setBounds(480, 50, 100, 25);
        add(nota1Field);

        nota2Label = new JLabel("Nota 2:");
        nota2Label.setBounds(420, 90, 50, 25);
        add(nota2Label);

        nota2Field = new JTextField();
        nota2Field.setBounds(480, 90, 100, 25);
        add(nota2Field);

        JButton salvarButton = new JButton("Salvar Avaliações");
        salvarButton.setBounds(10, 360, 150, 30);
        salvarButton.addActionListener(e -> salvarAvaliacoes());
        add(salvarButton);

        JButton voltarButton = new JButton("Voltar");
        voltarButton.setBounds(170, 360, 100, 30);
        voltarButton.addActionListener(e -> voltar());
        add(voltarButton);

        CrudDeEstudantes crudDeEstudantes = new CrudDeEstudantes("estudantes.dat");
        List<Estudante> estudantes = crudDeEstudantes.readAll();

        // Carrega as notas existentes
        List<Nota> notasExistentes = crudDeNotas.readAll();

        // Populate table with students
        for (Estudante estudante : estudantes) {
            Nota nota = notasExistentes.stream()
                    .filter(n -> n.getEstudanteId() == estudante.getId() && n.getTurmaId() == turma.getId())
                    .findFirst()
                    .orElseGet(() -> {
                        Nota novaNota = new Nota();
                        novaNota.setEstudanteId(estudante.getId());
                        novaNota.setTurmaId(turma.getId());
                        crudDeNotas.create(novaNota);
                        return novaNota;
                    });
            notasMap.put(estudante, nota);
            tableModel.addRow(new Object[]{estudante.getNome(), nota.getNota1(), nota.getNota2(), nota.getMedia()});
        }

        // Add table row click listener
        tabela.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int selectedRow = tabela.getSelectedRow();
                if (selectedRow >= 0) {
                    String nomeEstudante = (String) tabela.getValueAt(selectedRow, 0);
                    for (Estudante estudante : estudantes) {
                        if (estudante.getNome().equals(nomeEstudante)) {
                            estudanteSelecionado = estudante;
                            Nota nota = notasMap.get(estudante);
                            nota1Field.setText(nota.getNota1() == 0 ? "" : String.valueOf(nota.getNota1()));
                            nota2Field.setText(nota.getNota2() == 0 ? "" : String.valueOf(nota.getNota2()));
                            break;
                        }
                    }
                }
            }
        });
    }

    private void salvarAvaliacoes() {
        if (estudanteSelecionado == null) {
            JOptionPane.showMessageDialog(this, "Selecione um estudante na tabela.", "Erro", JOptionPane.ERROR_MESSAGE);
            return;
        }

        try {
            double nota1 = Double.parseDouble(nota1Field.getText());
            double nota2 = Double.parseDouble(nota2Field.getText());

            Nota nota = notasMap.get(estudanteSelecionado);
            nota.setNota1(nota1);
            nota.setNota2(nota2);
            nota.calcularMedia();

            // Update table
            DefaultTableModel model = (DefaultTableModel) tabela.getModel();
            for (int i = 0; i < model.getRowCount(); i++) {
                if (model.getValueAt(i, 0).equals(estudanteSelecionado.getNome())) {
                    model.setValueAt(nota.getNota1(), i, 1);
                    model.setValueAt(nota.getNota2(), i, 2);
                    model.setValueAt(nota.getMedia(), i, 3);
                    break;
                }
            }

            crudDeNotas.update(nota);

            JOptionPane.showMessageDialog(this, "Avaliações salvas com sucesso!", "Sucesso", JOptionPane.INFORMATION_MESSAGE);
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(this, "Notas inválidas.", "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void voltar() {
        Container parent = getParent();
        if (parent instanceof JFrame) {
            JFrame frame = (JFrame) parent;
            frame.dispose();
        }
    }
}

