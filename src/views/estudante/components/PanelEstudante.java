package views.estudante.components;

import app.models.Estudante;

import javax.swing.*;

/**
 * Created by USER on 25/05/2024.
 */

public class PanelEstudante extends JPanel {
    private Estudante estudante;

    public PanelEstudante(Estudante estudante) {
        this.estudante = estudante;
        initComponents();
    }

    private void initComponents() {
        // Inicialize e configure os componentes do painel para exibir informações sobre o estudante e suas notas
    }
}
