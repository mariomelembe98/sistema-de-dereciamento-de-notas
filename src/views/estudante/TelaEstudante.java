package views.estudante;

import app.models.Estudante;

import javax.swing.*;
import java.awt.*;

public class TelaEstudante extends JFrame {
    private Estudante estudante;

    public TelaEstudante(Estudante estudante) {
        this.estudante = estudante;

        setTitle("Área do Estudante");
        setSize(900, 500);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setLayout(new BorderLayout());
        setResizable(false);

        JLabel label = new JLabel("Bem-vindo, " + estudante.getNome() + "!", SwingConstants.CENTER);
        label.setFont(new Font("Arial", Font.BOLD, 24));
        add(label, BorderLayout.CENTER);

        // Adicione mais componentes e funcionalidades conforme necessário
    }

    public void exibir() {
        setVisible(true);
    }
}
