package app.services;

import app.models.Estudante;
import app.models.Professor;
import app.models.Usuario;

import java.util.List;

public class LoginService {
    private List<Usuario> usuarios;
    private List<Professor> professores;
    private List<Estudante> estudantes;

    // Construtor que recebe listas de usuários e professores
    public LoginService(List<Usuario> usuarios, List<Professor> professores, List<Estudante> estudantes) {
        this.usuarios = usuarios;
        this.professores = professores;
        this.estudantes = estudantes;
    }

    // Verifica as credenciais do usuário
    public Object verificarCredenciais(String username, String password) {
        for (Usuario usuario : usuarios) {
            if (usuario.getNomeUsuario().equals(username) && usuario.getSenha().equals(password)) {
                return usuario;
            }
        }
        for (Professor professor : professores) {
            if (professor.getNomeUsuario().equals(username) && professor.getSenha().equals(password)) {
                return professor;
            }
        }
        for (Estudante estudante : estudantes) {
            if (estudante.getNomeUsuario().equals(username) && estudante.getSenha().equals(password)){
                return estudante;
            }
        }
        return null; // Credenciais inválidas
    }
}
