package app.services;

import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.*;

public class EmailSender {
    public static void enviarEmail(String destinatario, String assunto, String mensagem) throws MessagingException {
        // TODO: Configurações do servidor SMTP
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.example.com"); // substitua pelo seu servidor SMTP
        props.put("mail.smtp.port", "587"); // porta SMTP
        props.put("mail.smtp.auth", "true"); // autenticação SMTP
        props.put("mail.smtp.starttls.enable", "true"); // habilita o STARTTLS

        // TODO: Credenciais de login
        String username = "mariojorgemelembe@gmail.com"; // substitua pelo seu e-mail
        String password = "Junho9884"; // substitua pela sua senha

        // TODO: Cria uma sessão com autenticação
        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        try {
            // TODO: Cria a mensagem de e-mail
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username)); // endereço de e-mail do remetente
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(destinatario)); // endereço de e-mail do destinatário
            message.setSubject(assunto); // assunto do e-mail
            message.setText(mensagem); // corpo do e-mail

            // TODO: Envia a mensagem
            Transport.send(message);

            System.out.println("E-mail enviado com sucesso para " + destinatario);
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
}
