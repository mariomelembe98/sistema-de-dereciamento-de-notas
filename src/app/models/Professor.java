package app.models;

import DAO.Autenticavel;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Professor implements Autenticavel, Serializable {
    private int id;
    private String nome;
    private String apelido;
    private String sexo;
    private String email;
    private String telefone;
    private String nomeUsuario;
    private String senha;
    private boolean status;
    private boolean isAdmin;
    private List<Integer> turmaIds;

    public Professor() {
        turmaIds = new ArrayList<>();
    }

    public Professor(int id, String nome, String apelido, String sexo, String email, String telefone, String nomeUsuario, String senha, boolean status, boolean isAdmin) {
        this.id = id;
        this.nome = nome;
        this.apelido = apelido;
        this.sexo = sexo;
        this.email = email;
        this.telefone = telefone;
        this.nomeUsuario = nomeUsuario;
        this.senha = senha;
        this.status = status;
        this.isAdmin = isAdmin;
        this.turmaIds = new ArrayList<>();
    }

    // Getters e Setters para todos os campos

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getApelido() {
        return apelido;
    }

    public void setApelido(String apelido) {
        this.apelido = apelido;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public List<Integer> getTurmaIds() {
        return turmaIds;
    }

    public void setTurmaIds(List<Integer> turmaIds) {
        this.turmaIds = turmaIds;
    }

    public void adicionarTurma(int turmaId) {
        turmaIds.add(turmaId);
    }

    public void removerTurma(int turmaId) {
        turmaIds.remove(Integer.valueOf(turmaId));
    }
}
