package app.models;

import DAO.Autenticavel;
import DAO.Crud;

import java.io.Serializable;
import java.util.List;

/**
 * Created by USER on 13/05/2024.
 */

public class Usuario implements Autenticavel, Serializable {
    private int id;
    private String nome;
    private String nomeUsuario;
    private String email;
    private String senha;
    private boolean admin;
    private boolean status;
    
    public Usuario(int id, String nome, String nomeUsuario , String email, String senha, boolean admin, boolean status) {
        this.id = id;
        this.nome = nome;
        this.nomeUsuario = nomeUsuario;
        this.email = email;
        this.senha = senha;
        this.admin = admin;
        this.status = status;
    }

    public Usuario() {

    }

    public Usuario(int ultimoId, String nome, String apelido, String email, boolean admin, boolean status) {
        this.id = ultimoId;
        this.nome = nome;
        this.nomeUsuario = apelido;
        this.email = email;
        this.admin = admin;
        this.status = status;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getNomeUsuario() {
        return nomeUsuario;
    }
    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getSenha() {
        return senha;
    }
    public void setSenha(String senha) {
        this.senha = senha;
    }
    public boolean isAdmin() {
        return admin;
    }
    public void setAdmin(boolean admin) {
        this.admin = admin;
    }
    public boolean getStatus() {
        return status;
    }
    public void setStatus(boolean status) {
        this.status = status;
    }
    
    
    @Override
    public String toString() {
        return "Id: "+ id +", Nome: " + nome + "Nome de Usuario: " + nomeUsuario + ", Email: " + email + ", Admin: " + admin + ", Status: " + status;
    }

    public static void main(String[] args) {
        Crud<Usuario> usuarioCrud = new Crud("usuarios.dat") {
            @Override
            public void update(Disciplina disciplina) {

            }

            @Override
            public void updates(Usuario usuario) {

            }

            @Override
            public void update(Usuario usuario) {

            }
        };


        // Ler todos os usuários
        List<Usuario> usuarios = usuarioCrud.readAll();
        for (Usuario usuario : usuarios) {
            System.out.println(usuario);
        }

        // Atualizar o primeiro usuário
        if (!usuarios.isEmpty()) {
            Usuario primeiroUsuario = usuarios.get(0);
            primeiroUsuario.setStatus(false);
            usuarioCrud.update(0, primeiroUsuario);
        }

        // Deletar o segundo usuário
        if (usuarios.size() > 1) {
            usuarioCrud.delete(1);
        }
    }

}
