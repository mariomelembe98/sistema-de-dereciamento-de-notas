package app.models;

import DAO.Autenticavel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Estudante implements Autenticavel, Serializable {
    private int id;
    private String codigo;
    private String nome;
    private String sobrenome;
    private String dataNascimento;
    private String sexo;
    private String email;
    private String telefone;
    private String turno;
    private String nivel;
    private String curso;
    private String nomeUsuario;
    private String senha;
    private List<Nota> avaliacoes;
    private int turmaId;
    private boolean admin;
    private boolean status;

    public Estudante() {
        this.avaliacoes = new ArrayList<>();
    }

    public Estudante(int id, String codigo, String nome, String sobrenome, String dataNascimento, String sexo, String email, String telefone, String turno, String nivel, String curso, String nomeUsuario, String senha, int turmaId, boolean admin, boolean status) {
        this.id = id;
        this.codigo = codigo;
        this.nome = nome;
        this.sobrenome = sobrenome;
        this.dataNascimento = dataNascimento;
        this.sexo = sexo;
        this.email = email;
        this.telefone = telefone;
        this.turno = turno;
        this.nivel = nivel;
        this.curso = curso;
        this.nomeUsuario = nomeUsuario;
        this.senha = senha;
        this.turmaId = turmaId;
        this.avaliacoes = new ArrayList<>();
        this.admin = admin;
        this.status = status;
    }

    public Estudante(int i, String s) {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public String getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public int getTurmaId() {
        return turmaId;
    }

    public void setTurmaId(int turmaId) {
        this.turmaId = turmaId;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public void addAvaliacao(Nota avaliacao) {
        this.avaliacoes.add(avaliacao);
    }

    public List<Nota> getAvaliacoes() {
        return avaliacoes;
    }
}
