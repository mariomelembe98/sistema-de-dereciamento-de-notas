package app.models;

public class TurmaProfessor {
    private int turmaId;
    private int professorId;

    public TurmaProfessor(int turmaId, int professorId) {
        this.turmaId = turmaId;
        this.professorId = professorId;
    }

    public int getTurmaId() {
        return turmaId;
    }

    public void setTurmaId(int turmaId) {
        this.turmaId = turmaId;
    }

    public int getProfessorId() {
        return professorId;
    }

    public void setProfessorId(int professorId) {
        this.professorId = professorId;
    }
}
