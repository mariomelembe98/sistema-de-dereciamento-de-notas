package app.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Turma implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String ano;
    private String nome;
    private List<Integer> professorIds;
    private String semestre;
    private String turno;

    public Turma() {
        professorIds = new ArrayList<>();
    }

    public Turma(int id, String nome, String ano, String semestre, String turno) {
        this.id = id;
        this.nome = nome;
        this.ano = ano;
        this.semestre = semestre;
        this.turno = turno;
        this.professorIds = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Integer> getProfessorIds() {
        return professorIds;
    }

    public void setProfessorIds(List<Integer> professorIds) {
        this.professorIds = professorIds;
    }

    public String getSemestre() {
        return semestre;
    }

    public void setSemestre(String semestre) {
        this.semestre = semestre;
    }

    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }

    public void adicionarProfessor(int professorId) {
        professorIds.add(professorId);
    }

    public void removerProfessor(int professorId) {
        professorIds.remove(Integer.valueOf(professorId));
    }
}
