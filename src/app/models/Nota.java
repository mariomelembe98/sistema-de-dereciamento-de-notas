package app.models;

import java.io.Serializable;

public class Nota implements Serializable {
    private int id;
    private int turmaId;
    private int estudanteId;
    private int disciplinaId;
    private double nota1;
    private double nota2;
    private double media;

    public Nota() {}

    public Nota(int id, int turmaId, int estudanteId, int disciplinaId, double nota1, double nota2) {
        this.id = id;
        this.turmaId = turmaId;
        this.estudanteId = estudanteId;
        this.disciplinaId = disciplinaId;
        this.nota1 = nota1;
        this.nota2 = nota2;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTurmaId() {
        return turmaId;
    }

    public void setTurmaId(int turmaId) {
        this.turmaId = turmaId;
    }

    public int getEstudanteId() {
        return estudanteId;
    }

    public void setEstudanteId(int estudanteId) {
        this.estudanteId = estudanteId;
    }

    public int getDisciplinaId() {
        return disciplinaId;
    }

    public void setDisciplinaId(int disciplinaId) {
        this.disciplinaId = disciplinaId;
    }

    public double getNota1() {
        return nota1;
    }

    public void setNota1(double nota1) {
        this.nota1 = nota1;
    }

    public double getNota2() {
        return nota2;
    }

    public void setNota2(double nota2) {
        this.nota2 = nota2;
    }

    public double getMedia() {
        return media;
    }

    public void setMedia(double media) {
        this.media = media;
    }

    public Double calcularMedia() {
        media = (nota1 + nota2) / 2;
        return media;
    }
}
