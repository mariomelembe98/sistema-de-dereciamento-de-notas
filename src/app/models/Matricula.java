package app.models;

import java.io.Serializable;

public class Matricula implements Serializable {

    private Integer codMatricula;
    private Integer codTurma;
    private Integer codAluno;
    private Estudante estudante;
    private Turma turma;

    public Integer getCodMatricula() {
        return codMatricula;
    }
    public void setCodMatricula(Integer codMatricula) {
        this.codMatricula = codMatricula;
    }

    public Integer getCodTurma() {
        return codTurma;
    }
    public void setCodTurma(Integer codTurma) {
        this.codTurma = codTurma;
    }

    public Integer getCodAluno() {
        return codAluno;
    }
    public void setCodAluno(Integer codAluno) {
        this.codAluno = codAluno;
    }

    public Estudante getEstudante() {
        return estudante;
    }
    public void setEstudante(Estudante estudante) {
        this.estudante = estudante;
    }

    public Turma getTurma() {
        return turma;
    }
    public void setTurma(Turma turma) {
        this.turma = turma;
    }
}