package controller;

import DAO.CrudDeCursos;
import app.models.Curso;
import raven.toast.Notifications;
import views.dashboard.components.PanelCursos;


import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class CursoController {
    private PanelCursos view;
    private CrudDeCursos model;

    public CursoController(PanelCursos view, CrudDeCursos model) {
        this.view = view;
        this.model = model;
        initController();
    }

    private void initController() {
        view.addSalvarButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                salvarCurso();
            }
        });

        view.addActualizarButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                atualizarCurso();
            }
        });

        view.addEliminarButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                eliminarCurso();
            }
        });

        view.addLimparButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                limparCampos();
            }
        });

        view.getCursosTable().getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    atualizarCampos();
                }
            }
        });

        atualizarTabela();
    }

    private void salvarCurso() {
        try {
            String nome = view.getNomeField().getText();
            String descricao = view.getDescricaoField().getText();


            if (nome.isEmpty()) {
                Notifications.getInstance().show(Notifications.Type.WARNING, Notifications.Location.TOP_CENTER, "Por favor, preencha o nome do curso.");
                return;
            }

            if (dadosDuplicados(nome, descricao)) {
                Notifications.getInstance().show(Notifications.Type.WARNING, Notifications.Location.TOP_CENTER, "Já existe um curso com o mesmo");
                return;
            }

            int id = model.readAll().size() + 1; // Autoincremento do ID

            Curso curso = new Curso(id, nome, descricao);
            model.create(curso);

            atualizarTabela();
            limparCampos();

            Notifications.getInstance().show(Notifications.Type.SUCCESS, Notifications.Location.TOP_CENTER, "Estudante salvo com sucesso!");
        } catch (Exception e) {
            e.printStackTrace();
            Notifications.getInstance().show(Notifications.Type.ERROR, Notifications.Location.TOP_CENTER, "Erro ao salvar estudante.");
        }
    }

    private boolean dadosDuplicados(String nome, String email) {
        List<Curso> cursos = model.readAll();
        for (Curso curso : cursos) {
            if (curso.getNome().equals(nome)) {
                return true;
            }
        }
        return false;
    }

    public void atualizarCurso() {
        try {
            int selectedRow = view.getCursosTable().getSelectedRow();
            if (selectedRow < 0) {
                Notifications.getInstance().show(Notifications.Type.WARNING, Notifications.Location.TOP_CENTER, "Selecione um estudante para atualizar.");
                return;
            }

            String nome = view.getNomeField().getText();
            String descricao = view.getDescricaoField().getText();

            if (nome.isEmpty()) {
                Notifications.getInstance().show(Notifications.Type.WARNING, Notifications.Location.TOP_CENTER, "Por favor, preencha todos os campos.");
                return;
            }

            int id = Integer.parseInt(view.getCodigoField().getText());
            Curso curso = new Curso(id, nome, descricao);
            model.update(curso);

            atualizarTabela();
            limparCampos();

            Notifications.getInstance().show(Notifications.Type.SUCCESS, Notifications.Location.TOP_CENTER, "Estudante atualizado com sucesso!");
        } catch (Exception e) {
            e.printStackTrace();
            Notifications.getInstance().show(Notifications.Type.ERROR, Notifications.Location.TOP_CENTER, "Erro ao atualizar estudante.");
        }
    }

    private void eliminarCurso() {
        try {
            int selectedRow = view.getCursosTable().getSelectedRow();
            if (selectedRow < 0) {
                Notifications.getInstance().show(Notifications.Type.WARNING, Notifications.Location.TOP_CENTER, "Selecione um estudante para eliminar.");
                return;
            }

            int id = Integer.parseInt(view.getCodigoField().getText());
            model.delete(id);

            atualizarTabela();
            limparCampos();

            Notifications.getInstance().show(Notifications.Type.SUCCESS, Notifications.Location.TOP_CENTER, "Estudante eliminado com sucesso!");
        } catch (Exception e) {
            e.printStackTrace();
            Notifications.getInstance().show(Notifications.Type.ERROR, Notifications.Location.TOP_CENTER, "Erro ao eliminar estudante.");
        }
    }

    private void atualizarCampos() {
        int selectedRow = view.getCursosTable().getSelectedRow();
        if (selectedRow >= 0) {
            view.getCodigoField().setText(view.getCursosTable().getValueAt(selectedRow, 0).toString());
            view.getNomeField().setText(view.getCursosTable().getValueAt(selectedRow, 1).toString());
            view.getDescricaoField().setText(view.getCursosTable().getValueAt(selectedRow, 2).toString());
        }
    }

    private void atualizarTabela() {
        DefaultTableModel model = (DefaultTableModel) view.getCursosTable().getModel();
        model.setRowCount(0);
        List<Curso> cursos = this.model.readAll();
        for (Curso curso : cursos) {
            model.addRow(new Object[]{
                    curso.getId(),
                    curso.getNome(),
                    curso.getDescricao()

            });
        }
    }

    private void limparCampos() {
        view.getCodigoField().setText("");
        view.getNomeField().setText("");
        view.getDescricaoField().setText("");

    }
}
