package controller;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

public class LoginController {

    private List<String> filePaths;

    // Construtor que aceita uma lista de caminhos de arquivos
    public LoginController(List<String> filePaths) {
        this.filePaths = filePaths;
    }

    // Método para autenticar o usuário
    public boolean autenticar(String username, String password) {
        for (String filePath : filePaths) {
            if (autenticarNoArquivo(username, password, filePath)) {
                return true; // Autenticação bem-sucedida
            }
        }
        return false; // Usuário ou senha incorretos
    }

    // TODO: Método auxiliar para autenticar em um único arquivo
    private boolean autenticarNoArquivo(String username, String password, String filePath) {
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.contains(":")) { // Verifica se a linha contém o caractere ':'
                    String[] parts = line.split(":");
                    if (parts.length == 2) { // Verifica se há exatamente dois elementos após a divisão
                        String storedUsername = parts[0];
                        String storedPassword = parts[1];
                        if (username.equals(storedUsername) && password.equals(storedPassword)) {
                            return true; // Autenticação bem-sucedida
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "Erro no arquivo de usuários: linha malformada em " + filePath, "Erro", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Erro ao ler o arquivo de usuários: " + filePath, "Erro", JOptionPane.ERROR_MESSAGE);
        }
        return false; // Usuário ou senha incorretos
    }
}
