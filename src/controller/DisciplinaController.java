package controller;

import DAO.CrudDeDisciplinas;
import app.models.Disciplina;
import raven.toast.Notifications;
import views.dashboard.components.PanelDisciplinas;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class DisciplinaController {
    private PanelDisciplinas panelDisciplinas;
    private CrudDeDisciplinas disciplinaCrud;
    private int ultimoId = 0;

    public DisciplinaController(PanelDisciplinas panelDisciplinas, CrudDeDisciplinas disciplinaCrud) {
        this.panelDisciplinas = panelDisciplinas;
        this.disciplinaCrud = disciplinaCrud;

        // Configura os ouvintes para os botões
        panelDisciplinas.getSalvarButton().addActionListener(new SalvarListener());
        panelDisciplinas.getLimparButton().addActionListener(new LimparListener());
        panelDisciplinas.getCarregarButton().addActionListener(new CarregarListener());
        panelDisciplinas.getAtualizarButton().addActionListener(new AtualizarListener());
        panelDisciplinas.getEliminarButton().addActionListener(new EliminarListener());

        atualizarTabela();
    }

    // Listener para o botão de salvar
    class SalvarListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String nome = panelDisciplinas.getNomeField().getText();
            String curso = (String) panelDisciplinas.getCursoCombobox().getSelectedItem();

            if (!nome.isEmpty() && !curso.isEmpty()) {
                ultimoId = disciplinaCrud.getNextId();
                Disciplina novaDisciplina = new Disciplina(ultimoId, nome, curso);
                disciplinaCrud.create(novaDisciplina);
                atualizarTabela();
                limparCampos();
                Notifications.getInstance().show(Notifications.Type.SUCCESS, Notifications.Location.TOP_CENTER, "Disciplina registrada com sucesso!");
            } else {
                Notifications.getInstance().show(Notifications.Type.WARNING, Notifications.Location.TOP_CENTER, "Preencha todos os campos!");
            }
        }
    }

    // Listener para o botão de limpar
    class LimparListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            limparCampos();
        }
    }

    // Listener para o botão de carregar
    class CarregarListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            atualizarTabela();
        }
    }

    // Listener para o botão de atualizar
    class AtualizarListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            int selectedRow = panelDisciplinas.getDisciplinasTable().getSelectedRow();
            if (selectedRow >= 0) {
                int id = (int) panelDisciplinas.getDisciplinasTable().getValueAt(selectedRow, 0);
                String nome = panelDisciplinas.getNomeField().getText();
                String curso = (String) panelDisciplinas.getCursoCombobox().getSelectedItem();

                if (!nome.isEmpty() && !curso.isEmpty()) {
                    Disciplina disciplinaAtualizada = new Disciplina(id, nome, curso);
                    disciplinaCrud.update(disciplinaAtualizada);
                    atualizarTabela();
                    limparCampos();
                    Notifications.getInstance().show(Notifications.Type.SUCCESS, Notifications.Location.TOP_CENTER, "Disciplina atualizada com sucesso!");
                } else {
                    Notifications.getInstance().show(Notifications.Type.WARNING, Notifications.Location.TOP_CENTER, "Preencha todos os campos!");
                }
            } else {
                JOptionPane.showMessageDialog(panelDisciplinas, "Selecione uma disciplina na tabela para atualizar.");
            }
        }
    }

    // Listener para o botão de eliminar
    class EliminarListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            int selectedRow = panelDisciplinas.getDisciplinasTable().getSelectedRow();
            if (selectedRow >= 0) {
                int id = (int) panelDisciplinas.getDisciplinasTable().getValueAt(selectedRow, 0);
                disciplinaCrud.delete(id);
                atualizarTabela();
                limparCampos();
                Notifications.getInstance().show(Notifications.Type.SUCCESS, Notifications.Location.TOP_CENTER, "Disciplina eliminada com sucesso!");
            } else {
                JOptionPane.showMessageDialog(panelDisciplinas, "Selecione uma disciplina na tabela para eliminar.");
            }
        }
    }

    // Atualiza a tabela de disciplinas
    public void atualizarTabela() {
        List<Disciplina> disciplinas = disciplinaCrud.readAll();
        DefaultTableModel tableModel = (DefaultTableModel) panelDisciplinas.getDisciplinasTable().getModel();
        tableModel.setRowCount(0);

        for (Disciplina d : disciplinas) {
            tableModel.addRow(new Object[]{
                    d.getId(),
                    d.getNome(),
                    d.getCurso()
            });
        }
    }

    // Limpa os campos de entrada
    private void limparCampos() {
        panelDisciplinas.getNomeField().setText("");
        panelDisciplinas.getCursoCombobox().setSelectedIndex(0);
    }
}
