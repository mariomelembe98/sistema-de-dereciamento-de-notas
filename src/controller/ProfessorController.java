package controller;

import DAO.CrudDeProfessores;
import app.models.Professor;
import app.models.Turma;
import raven.toast.Notifications;
import views.dashboard.components.PanelProfessores;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class ProfessorController {
    private PanelProfessores view;
    private CrudDeProfessores model;

    public ProfessorController(PanelProfessores view, CrudDeProfessores model) {
        this.view = view;
        this.model = model;
        initController();
    }

    private void initController() {
        view.getSalvarButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                salvarProfessor();
            }
        });

        view.getEliminarButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                eliminarProfessor();
            }
        });

        view.getActualizarButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                atualizarProfessor();
            }
        });

        view.getLimparButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                limparCampos();
            }
        });

        view.getProfessoresTable().getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    atualizarCampos();
                }
            }
        });

        atualizarTabela();
    }

    private void salvarProfessor() {
        try {
            // Validações e conversão dos campos
            String nome = view.getNomeField().getText().trim();
            String apelido = view.getApelidoField().getText().trim();
            String sexo = (String) view.getSexoComboBox().getSelectedItem();
            String email = view.getEmailField().getText().trim();
            String telefone = view.getTelefoneField().getText().trim();
            String nomeUsuario = view.getNomeUsuarioField().getText().trim();
            String senha = new String(view.getPasswordField().getPassword());
            String confirmarSenha = new String(view.getConfirmarPasswordField().getPassword());
            boolean status = view.getStatusCheckBox().isSelected();
            boolean isAdmin = view.getAdminCheckBox().isSelected();

            if (nome.isEmpty() || apelido.isEmpty() || sexo.isEmpty() || email.isEmpty() || telefone.isEmpty()) {
                Notifications.getInstance().show(Notifications.Type.WARNING, Notifications.Location.TOP_CENTER, "Por favor, preencha todos os campos.");
                return;
            }

            int id = model.readAll().size() + 1; // Autoincremento do ID

            Professor professor = new Professor(id, nome, apelido, sexo, email, telefone, nomeUsuario, senha, status, isAdmin);

            // Adiciona as turmas associadas ao professor
            List<Turma> turmasSelecionadas = view.getSelectedTurmas();
            List<Integer> turmaIds = new ArrayList<>();
            for (Turma turma : turmasSelecionadas) {
                turmaIds.add(turma.getId());
            }
            professor.setTurmaIds(turmaIds);

            model.create(professor);

            Notifications.getInstance().show(Notifications.Type.SUCCESS, Notifications.Location.TOP_CENTER, "Professor salvo com sucesso.");
            atualizarTabela();
            limparCampos();
        } catch (Exception e) {
            e.printStackTrace();
            Notifications.getInstance().show(Notifications.Type.ERROR, Notifications.Location.TOP_CENTER, "Erro ao salvar o professor.");
        }
    }

    private void eliminarProfessor() {
        try {
            String idText = view.getIdTextField().getText().trim();
            if (idText.isEmpty()) {
                Notifications.getInstance().show(Notifications.Type.WARNING, Notifications.Location.TOP_CENTER, "Por favor, selecione um professor para eliminar.");
                return;
            }

            int id = Integer.parseInt(idText);
            model.delete(id);

            Notifications.getInstance().show(Notifications.Type.SUCCESS, Notifications.Location.TOP_CENTER, "Professor eliminado com sucesso.");
            atualizarTabela();
            limparCampos();
        } catch (NumberFormatException e) {
            Notifications.getInstance().show(Notifications.Type.ERROR, Notifications.Location.TOP_CENTER, "ID inválido.");
        } catch (Exception e) {
            e.printStackTrace();
            Notifications.getInstance().show(Notifications.Type.ERROR, Notifications.Location.TOP_CENTER, "Erro ao eliminar o professor.");
        }
    }

    private void atualizarProfessor() {
        try {
            String idText = view.getIdTextField().getText().trim();
            if (idText.isEmpty()) {
                Notifications.getInstance().show(Notifications.Type.WARNING, Notifications.Location.TOP_CENTER, "Por favor, selecione um professor para atualizar.");
                return;
            }

            int id = Integer.parseInt(idText);

            // Validações e conversão dos campos
            String nome = view.getNomeField().getText().trim();
            String apelido = view.getApelidoField().getText().trim();
            String sexo = (String) view.getSexoComboBox().getSelectedItem();
            String email = view.getEmailField().getText().trim();
            String telefone = view.getTelefoneField().getText().trim();
            String nomeUsuario = view.getNomeUsuarioField().getText().trim();
            String senha = new String(view.getPasswordField().getPassword());
            String confirmarSenha = new String(view.getConfirmarPasswordField().getPassword());
            boolean status = view.getStatusCheckBox().isSelected();
            boolean isAdmin = view.getAdminCheckBox().isSelected();

            if (nome.isEmpty() || apelido.isEmpty() || sexo.isEmpty() || email.isEmpty() || telefone.isEmpty()) {Notifications.getInstance().show(Notifications.Type.WARNING, Notifications.Location.TOP_CENTER, "Por favor, preencha todos os campos.");
                return;
            }

            Professor professor = new Professor(id, nome, apelido, sexo, email, telefone, nomeUsuario, senha, status, isAdmin);

            // Adiciona as turmas associadas ao professor
            List<Turma> turmasSelecionadas = view.getSelectedTurmas();
            List<Integer> turmaIds = new ArrayList<>();
            for (Turma turma : turmasSelecionadas) {
                turmaIds.add(turma.getId());
            }
            professor.setTurmaIds(turmaIds);

            model.update(professor);

            Notifications.getInstance().show(Notifications.Type.SUCCESS, Notifications.Location.TOP_CENTER, "Professor atualizado com sucesso.");
            atualizarTabela();
            limparCampos();
        } catch (NumberFormatException e) {
            Notifications.getInstance().show(Notifications.Type.ERROR, Notifications.Location.TOP_CENTER, "ID inválido.");
        } catch (Exception e) {
            e.printStackTrace();
            Notifications.getInstance().show(Notifications.Type.ERROR, Notifications.Location.TOP_CENTER, "Erro ao atualizar o professor.");
        }
    }

    private void atualizarCampos() {
        int selectedRow = view.getProfessoresTable().getSelectedRow();
        if (selectedRow != -1) {
            DefaultTableModel model = view.getTableModel();
            view.getIdTextField().setText(model.getValueAt(selectedRow, 0).toString());
            view.getNomeField().setText(model.getValueAt(selectedRow, 1).toString());
            view.getApelidoField().setText(model.getValueAt(selectedRow, 2).toString());
            view.getSexoComboBox().setSelectedItem(model.getValueAt(selectedRow, 3).toString());
            view.getEmailField().setText(model.getValueAt(selectedRow, 4).toString());
            view.getTelefoneField().setText(model.getValueAt(selectedRow, 5).toString());
            view.getNomeUsuarioField().setText(model.getValueAt(selectedRow, 6).toString());
            view.getStatusCheckBox().setSelected(Boolean.parseBoolean(model.getValueAt(selectedRow, 7).toString()));
            view.getAdminCheckBox().setSelected(Boolean.parseBoolean(model.getValueAt(selectedRow, 8).toString()));

            // Carrega as turmas associadas ao professor selecionado
            int professorId = Integer.parseInt(model.getValueAt(selectedRow, 0).toString());
//            List<Turma> turmas = model.getTurmasOfProfessor(professorId);
//            view.getTurmaList().setListData(turmas.toArray(new Turma[0]));
        }
    }

    private void limparCampos() {
        view.getIdTextField().setText("");
        view.getNomeField().setText("");
        view.getApelidoField().setText("");
        view.getSexoComboBox().setSelectedIndex(0);
        view.getEmailField().setText("");
        view.getTelefoneField().setText("");
        view.getNomeUsuarioField().setText("");
        view.getPasswordField().setText("");
        view.getConfirmarPasswordField().setText("");
        view.getStatusCheckBox().setSelected(true);
        view.getAdminCheckBox().setSelected(false);
        view.getSelectedTurmas();
    }

    private void atualizarTabela() {
        DefaultTableModel model = view.getTableModel();
        model.setRowCount(0); // Limpa a tabela antes de atualizar

        List<Professor> professores = this.model.readAll();
        for (Professor professor : professores) {
            Object[] row = {
                    professor.getId(),
                    professor.getNome(),
                    professor.getApelido(),
                    professor.getSexo(),
                    professor.getEmail(),
                    professor.getTelefone(),
                    professor.getNomeUsuario(),
                    professor.getStatus(),
                    professor.isAdmin(),
                    professor.getTurmaIds().toString()
            };
            model.addRow(row);
        }
    }
}
