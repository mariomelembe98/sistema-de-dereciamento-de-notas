package controller;

import DAO.CrudDeTurmas;
import app.models.Professor;
import app.models.Turma;
import raven.toast.Notifications;
import views.dashboard.components.PanelTurmas;

import java.util.List;

public class TurmaController {
    private CrudDeTurmas turmasCrud;
    private PanelTurmas panelTurmas;

    public TurmaController(PanelTurmas panelTurmas, CrudDeTurmas turmasCrud) {
        this.panelTurmas = panelTurmas;
        this.turmasCrud = turmasCrud;
        this.panelTurmas.setController(this);
        atualizarTabela();
    }

    public void salvarTurma(String nome, String ano, String semestre, String turno, List<Professor> professores) {
        try {
            if (nome.isEmpty() || ano.isEmpty() || semestre.isEmpty() || turno.isEmpty() || professores.isEmpty()) {
                Notifications.getInstance().show(Notifications.Type.WARNING, Notifications.Location.TOP_CENTER, "Por favor, preencha todos os campos.");
                return;
            }

            Turma turma = new Turma(turmasCrud.getNextId(), nome, ano, semestre, turno);
            for (Professor professor : professores) {
                turma.adicionarProfessor(professor.getId());
            }

            turmasCrud.create(turma);
            Notifications.getInstance().show(Notifications.Type.SUCCESS, Notifications.Location.TOP_CENTER, "Turma " + turma.getNome() + " criada com sucesso!");
            atualizarTabela();
            panelTurmas.limparCampos();
        } catch (NumberFormatException ex) {
            Notifications.getInstance().show(Notifications.Type.WARNING, Notifications.Location.TOP_CENTER, "Por favor, insira valores válidos.");
        }
    }

    public void atualizarTurma(int turmaSelecionadaId, String nome, String ano, String semestre, String turno, List<Professor> professores) {
        try {
            if (nome.isEmpty() || ano.isEmpty() || semestre.isEmpty() || turno.isEmpty() || professores.isEmpty()) {
                Notifications.getInstance().show(Notifications.Type.WARNING, Notifications.Location.TOP_CENTER, "Por favor, preencha todos os campos.");
                return;
            }

            Turma turma = new Turma(turmaSelecionadaId, nome, ano, semestre, turno);
            for (Professor professor : professores) {
                turma.adicionarProfessor(professor.getId());
            }

            turmasCrud.update(turma);
            Notifications.getInstance().show(Notifications.Type.SUCCESS, Notifications.Location.TOP_CENTER, "Turma " + turma.getNome() + " atualizada com sucesso!");
            atualizarTabela();
            panelTurmas.limparCampos();
        } catch (NumberFormatException ex) {
            Notifications.getInstance().show(Notifications.Type.WARNING, Notifications.Location.TOP_CENTER, "Por favor, insira valores válidos.");
        }
    }

    public void excluirTurma(int turmaSelecionadaId) {
        turmasCrud.delete(turmaSelecionadaId);
        atualizarTabela();
        panelTurmas.limparCampos();
    }

    public void atualizarTabela() {
        List<Turma> turmas = turmasCrud.readAll();
        panelTurmas.atualizarTabela(turmas);
    }
}
