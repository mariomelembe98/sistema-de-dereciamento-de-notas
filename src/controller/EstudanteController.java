package controller;

import DAO.CrudDeEstudantes;
import app.models.Estudante;
import raven.toast.Notifications;
import views.dashboard.components.PanelEstudantes;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

public class EstudanteController {
    private PanelEstudantes view;
    private CrudDeEstudantes model;

    public EstudanteController(PanelEstudantes view, CrudDeEstudantes model) {
        this.view = view;
        this.model = model;
        initController();
    }

    private void initController() {
        view.addSalvarButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                salvarEstudante();
            }
        });

        view.addActualizarButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                atualizarEstudante();
            }
        });

        view.addEliminarButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                eliminarEstudante();
            }
        });

        view.addLimparButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                limparCampos();
            }
        });

        view.getEstudantesTable().getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    atualizarCampos();
                }
            }
        });

        atualizarTabela();
    }

    private void salvarEstudante() {
        try {
            String nome = view.getNomeTextField().getText();
            String sobrenome = view.getSobrenomeTextField().getText();
            String dataNascimento = view.getDataNascimentoTextField().getText();
            String sexo = (String) view.getSexoComboBox().getSelectedItem();
            String email = view.getEmailTextField().getText();
            String telefone = view.getTelefoneTextField().getText();
            String turno = (String) view.getTurnoCombobox().getSelectedItem();
            String nivel = (String) view.getNivelCombobox().getSelectedItem();
            String curso = (String) view.getCursoComboBox().getSelectedItem();
            String nomeUsuario = view.getNomeUsuarioTextField().getText();
            String senha = view.getSenhaTextField().getText();
            boolean admin = view.getAdminCheckBox().isSelected();
            boolean status = view.getStatusCheckBox().isSelected();

            int id = model.readAll().size() + 1;
            LocalDate dataAtual = LocalDate.now();

            String codico = "CE-" + id + dataAtual.getYear();

            if (nome.isEmpty() || sobrenome.isEmpty() || dataNascimento.isEmpty() || sexo.isEmpty() || email.isEmpty() || telefone.isEmpty() || turno.isEmpty() || curso.isEmpty() || nomeUsuario.isEmpty() || senha.isEmpty()) {
                Notifications.getInstance().show(Notifications.Type.WARNING, Notifications.Location.TOP_CENTER, "Por favor, preencha todos os campos.");
                return;
            }

            if (dadosDuplicados(codico, email, nomeUsuario)) {
                Notifications.getInstance().show(Notifications.Type.WARNING, Notifications.Location.TOP_CENTER, "Já existe um estudante com o mesmo codico, email ou nome de usuario.");
                return;
            }else {

                Estudante estudante = new Estudante(id, codico, nome, sobrenome, dataNascimento, sexo, email, telefone, turno, nivel, curso, nomeUsuario, senha, 2,admin, status);
                model.create(estudante);
                atualizarTabela();
                limparCampos();
                Notifications.getInstance().show(Notifications.Type.SUCCESS, Notifications.Location.TOP_CENTER, "Estudante salvo com sucesso!");

            }


        } catch (Exception e) {
            e.printStackTrace();
            Notifications.getInstance().show(Notifications.Type.ERROR, Notifications.Location.TOP_CENTER, "Erro ao salvar estudante.");
        }
    }

    private boolean dadosDuplicados(String codico, String email, String nomeUsuario) {
        List<Estudante> estudantes = model.readAll();
        for (Estudante estudante : estudantes) {
            if (estudante.getCodigo().equals(codico) && estudante.getEmail().equals(email) && estudante.getNomeUsuario().equals(nomeUsuario)) {
                return true;
            }
        }
        return false;
    }

    public void atualizarEstudante() {
        try {
            int selectedRow = view.getEstudantesTable().getSelectedRow();
            if (selectedRow < 0) {
                Notifications.getInstance().show(Notifications.Type.WARNING, Notifications.Location.TOP_CENTER, "Selecione um estudante para atualizar.");
                return;
            }

            String nome = view.getNomeTextField().getText();
            String codico = view.getCodigoTextField().getText();
            String sobrenome = view.getSobrenomeTextField().getText();
            String dataNascimento = view.getDataNascimentoTextField().getText();
            String sexo = (String) view.getSexoComboBox().getSelectedItem();
            String email = view.getEmailTextField().getText();
            String telefone = view.getTelefoneTextField().getText();
            String turno = (String) view.getTurnoCombobox().getSelectedItem();
            String nivel = (String) view.getNivelCombobox().getSelectedItem();
            String curso = (String) view.getCursoComboBox().getSelectedItem();
            String nomeUsuario = view.getNomeUsuarioTextField().getText();
            String senha = view.getSenhaTextField().getText();
            boolean admin = view.getAdminCheckBox().isSelected();
            boolean status = view.getStatusCheckBox().isSelected();

            if (nome.isEmpty() || sobrenome.isEmpty() || dataNascimento.isEmpty() || sexo.isEmpty() || email.isEmpty() || telefone.isEmpty() || turno.isEmpty() || curso.isEmpty() || nomeUsuario.isEmpty() || senha.isEmpty()) {
                Notifications.getInstance().show(Notifications.Type.WARNING, Notifications.Location.TOP_CENTER, "Por favor, preencha todos os campos.");
                return;
            }

            if (dadosDuplicados(codico, email, nomeUsuario)) {
                Notifications.getInstance().show(Notifications.Type.WARNING, Notifications.Location.TOP_CENTER, "Já existe um estudante com o mesmo nome, email ou nome de usuario.");
                return;
            }else {

                int id = Integer.parseInt(view.getIdTextField().getText());
                Estudante estudante = new Estudante(id, codico, nome, sobrenome, dataNascimento, sexo, email, telefone, turno, nivel, curso, nomeUsuario, senha, 2,admin, status);
                model.update(estudante);

                atualizarTabela();
                limparCampos();

                Notifications.getInstance().show(Notifications.Type.SUCCESS, Notifications.Location.TOP_CENTER, "Estudante atualizado com sucesso!");

            }

        } catch (Exception e) {
            e.printStackTrace();
            Notifications.getInstance().show(Notifications.Type.ERROR, Notifications.Location.TOP_CENTER, "Erro ao atualizar estudante.");
        }
    }

    private void eliminarEstudante() {
        try {
            int selectedRow = view.getEstudantesTable().getSelectedRow();
            if (selectedRow < 0) {
                Notifications.getInstance().show(Notifications.Type.WARNING, Notifications.Location.TOP_CENTER, "Selecione um estudante para eliminar.");
                return;
            }

            int id = Integer.parseInt(view.getIdTextField().getText());

            int confirmacao = JOptionPane.showConfirmDialog(null, "Deseja realmente eliminar este estudante", "Sim", JOptionPane.YES_NO_OPTION);
            if (confirmacao == JOptionPane.YES_OPTION) {
                model.delete(id);
                Notifications.getInstance().show(Notifications.Type.SUCCESS, Notifications.Location.TOP_CENTER, "Estudante elimanado com sucesso!");

                atualizarTabela();
                limparCampos();

            }


        } catch (Exception e) {
            e.printStackTrace();
            Notifications.getInstance().show(Notifications.Type.ERROR, Notifications.Location.TOP_CENTER, "Erro ao eliminar estudante.");
        }
    }

    private void atualizarCampos() {
        int selectedRow = view.getEstudantesTable().getSelectedRow();
        if (selectedRow >= 0) {
            view.getIdTextField().setText(view.getEstudantesTable().getValueAt(selectedRow, 0).toString());
            view.getCodigoTextField().setText(view.getEstudantesTable().getValueAt(selectedRow, 1).toString());
            view.getNomeTextField().setText(view.getEstudantesTable().getValueAt(selectedRow, 2).toString());
            view.getSobrenomeTextField().setText(view.getEstudantesTable().getValueAt(selectedRow, 3).toString());
            view.getDataNascimentoTextField().setText(view.getEstudantesTable().getValueAt(selectedRow, 4).toString());
            view.getSexoComboBox().setSelectedItem(view.getEstudantesTable().getValueAt(selectedRow, 5).toString());
            view.getEmailTextField().setText(view.getEstudantesTable().getValueAt(selectedRow, 6).toString());
            view.getTelefoneTextField().setText(view.getEstudantesTable().getValueAt(selectedRow, 7).toString());
            view.getTurnoCombobox().setSelectedItem(view.getEstudantesTable().getValueAt(selectedRow, 8).toString());
            view.getNivelCombobox().setSelectedItem(view.getEstudantesTable().getValueAt(selectedRow, 9).toString());
            view.getCursoComboBox().setSelectedItem(view.getEstudantesTable().getValueAt(selectedRow, 10).toString());
            view.getNomeUsuarioTextField().setText(view.getEstudantesTable().getValueAt(selectedRow, 11).toString());
            //view.getSenhaTextField().setText(view.getEstudantesTable().getValueAt(selectedRow, 12).toString());
            view.getStatusCheckBox().setSelected(view.getEstudantesTable().getValueAt(selectedRow, 12).toString().equals("Ativo"));
        }
    }

    private void atualizarTabela() {
        DefaultTableModel model = (DefaultTableModel) view.getEstudantesTable().getModel();
        model.setRowCount(0);
        List<Estudante> estudantes = this.model.readAll();
        for (Estudante estudante : estudantes) {
            model.addRow(new Object[]{
                    estudante.getId(),
                    estudante.getCodigo(),
                    estudante.getNome(),
                    estudante.getSobrenome(),
                    estudante.getDataNascimento(),
                    estudante.getSexo(),
                    estudante.getEmail(),
                    estudante.getTelefone(),
                    estudante.getTurno(),
                    estudante.getNivel(),
                    estudante.getCurso(),
                    estudante.getNomeUsuario(),
                    //estudante.getSenha(),
                    estudante.getStatus() ? "Activo" : "Inativo"
            });
        }
    }

    private void limparCampos() {
        view.getIdTextField().setText("");
        view.getCodigoTextField().setText("");
        view.getNomeTextField().setText("");
        view.getSobrenomeTextField().setText("");
        view.getDataNascimentoTextField().setText("");
        view.getSexoComboBox().setSelectedIndex(0);
        view.getEmailTextField().setText("");
        view.getTelefoneTextField().setText("");
        view.getTurnoCombobox().setSelectedIndex(0);
        view.getCursoComboBox().setSelectedIndex(0);
        view.getNomeUsuarioTextField().setText("");
        view.getSenhaTextField().setText("");
        view.getTurmaCombobox().setSelectedIndex(0);
        view.getAdminCheckBox().setSelected(false);
        view.getStatusCheckBox().setSelected(false);
    }
}
