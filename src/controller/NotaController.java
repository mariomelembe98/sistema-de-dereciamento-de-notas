package controller;

import DAO.CrudDeNotas;
import app.models.Nota;
import raven.toast.Notifications;
import validation.ValidationUtils;
import views.dashboard.components.PanelNotas;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class NotaController {
    private PanelNotas panelNotas;
    private CrudDeNotas crudDeNotas;
    private int ultimoId = 0;

    public NotaController(PanelNotas panelNotas, CrudDeNotas crudDeNotas) {
        this.panelNotas = panelNotas;
        this.crudDeNotas = crudDeNotas;
        initController();
    }

    private void initController() {
        panelNotas.getBtnSalvar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                salvarNota();
            }
        });

        panelNotas.getBtnAtualizar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                atualizarNota();
            }
        });

        panelNotas.getBtnEliminar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                eliminarNota();
            }
        });

        carregarNotas();
    }

    private void salvarNota() {
        String turma = (String) panelNotas.getCbTurma().getSelectedItem();
        String estudante = (String) panelNotas.getCbEstudante().getSelectedItem();
        String disciplina = (String) panelNotas.getCbDisciplina().getSelectedItem();

        String nota1Text = panelNotas.getTxtNota1().getText();
        String nota2Text = panelNotas.getTxtNota2().getText();

        if (nota1Text.equals("") || nota2Text.equals("")) {
            Notifications.getInstance().show(Notifications.Type.WARNING, Notifications.Location.TOP_CENTER, "Por favor, preecha todos os campos");
        }else {

            if (!ValidationUtils.isNumero(nota1Text) || !ValidationUtils.isNumero(nota2Text)) {
                Notifications.getInstance().show(Notifications.Type.WARNING, Notifications.Location.TOP_CENTER, "As notas devem ser números válidos.");
                return;
            }

            double nota1 = Double.parseDouble(nota1Text);
            double nota2 = Double.parseDouble(nota2Text);

            if (nota1 < 0 || nota2 < 0 || nota1 > 20 || nota2 > 20) {
                Notifications.getInstance().show(Notifications.Type.WARNING, Notifications.Location.TOP_CENTER, "A nota deve estar entre 0 e 20.");
                return; // Aborta a operação de salvar
            }

            ultimoId++;
//            Nota nota = new Nota(ultimoId, turma, estudante, disciplina, nota1, nota2);
//            crudDeNotas.create(nota);
            Notifications.getInstance().show(Notifications.Type.SUCCESS, Notifications.Location.TOP_CENTER, "Notas adicionadas com sucesso!");
            carregarNotas();
        }


    }


    private void atualizarNota() {
        int selectedRow = panelNotas.getNotasTable().getSelectedRow();
        if (selectedRow >= 0) {
            int id = (int) panelNotas.getTableModel().getValueAt(selectedRow, 0);
            String turma = (String) panelNotas.getCbTurma().getSelectedItem();
            String estudante = (String) panelNotas.getCbEstudante().getSelectedItem();
            String disciplina = (String) panelNotas.getCbDisciplina().getSelectedItem();
            double nota1 = Double.parseDouble(panelNotas.getTxtNota1().getText());
            double nota2 = Double.parseDouble(panelNotas.getTxtNota2().getText());

            if (nota1 > 20 || nota2 > 20) {
                Notifications.getInstance().show(Notifications.Type.WARNING, Notifications.Location.TOP_CENTER, "A nota não pode ser maior que 20.");
                return; // TODO: Aborta a operação de actualizar
            }

//            Nota nota = new Nota(id, turma, estudante, disciplina, nota1, nota2);
//            crudDeNotas.update(nota);
            Notifications.getInstance().show(Notifications.Type.SUCCESS, Notifications.Location.TOP_CENTER, "Notas actualizadas com sucesso!");
            carregarNotas();
        }
    }

    private void eliminarNota() {
        int selectedRow = panelNotas.getNotasTable().getSelectedRow();
        if (selectedRow >= 0) {
            int id = (int) panelNotas.getTableModel().getValueAt(selectedRow, 0);
            int confirmacao = JOptionPane.showConfirmDialog(null, "Deseja realmente eliminar esta avaliacao", "Sim", JOptionPane.YES_NO_OPTION);
            if (confirmacao == JOptionPane.YES_OPTION) {
                crudDeNotas.delete(id);
                Notifications.getInstance().show(Notifications.Type.SUCCESS, Notifications.Location.TOP_CENTER, "Avaliacao elimanada com sucesso!");
            }
            carregarNotas();
        }
    }

    private void carregarNotas() {
        DefaultTableModel model = panelNotas.getTableModel();
        model.setRowCount(0);

        for (Nota nota : crudDeNotas.readAll()) {
            model.addRow(new Object[]{
                    nota.getId(),
//                    nota.getEstudante(),
//                    nota.getDisciplina(),
                    nota.getNota1(),
                    nota.getNota2(),
                    nota.calcularMedia()
            });
        }
    }
}
