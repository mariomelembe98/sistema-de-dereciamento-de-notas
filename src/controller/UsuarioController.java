package controller;

import DAO.CrudDeUsuarios;
import app.models.Usuario;
import raven.toast.Notifications;
import validation.ValidationUtils;
import views.dashboard.components.PanelUsuarios;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.function.Predicate;

public class UsuarioController {
    private PanelUsuarios view;
    private CrudDeUsuarios usuarioCrud;
    private int ultimoId = 0;

    public UsuarioController(PanelUsuarios panelUsuarios, CrudDeUsuarios usuarioCrud) {
        this.view = panelUsuarios;
        this.usuarioCrud = usuarioCrud;

        // Adiciona os listeners aos botões do painel
        panelUsuarios.getSalvarButton().addActionListener(new SalvarListener());
        panelUsuarios.getLimparButton().addActionListener(new LimparListener());
        panelUsuarios.getCarregarButton().addActionListener(new CarregarListener());
        panelUsuarios.getActualizarButton().addActionListener(new AtualizarListener());
        panelUsuarios.getEliminarButton().addActionListener(new EliminarListener());
        panelUsuarios.getPesquisarButton().addActionListener(new PesquisarListener());

        // Adiciona o listener para a seleção da tabela
        panelUsuarios.getUsuariosTable().getSelectionModel().addListSelectionListener(new SelecionarListener());

        // Carrega os usuários existentes
        atualizarTabela();

        // Inicializa ultimoId com o maior ID existente
        inicializarUltimoId();
    }

    // Inicializa ultimoId com o maior ID existente
    private void inicializarUltimoId() {
        List<Usuario> usuarios = usuarioCrud.readAll();
        for (Usuario usuario : usuarios) {
            if (usuario.getId() > ultimoId) {
                ultimoId = usuario.getId();
            }
        }
    }

    private boolean nomeUsuarioExiste(String nomeUsuario, int idIgnorar) {
        List<Usuario> usuarios = usuarioCrud.readAll();
        for (Usuario usuario : usuarios) {
            if (usuario.getNomeUsuario().equals(nomeUsuario) && usuario.getId() != idIgnorar) {
                return true;
            }
        }
        return false;
    }

    // Listener para o botão de salvar
    class SalvarListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String nome = view.getNomeField().getText();
            String nomeUsuario = view.getNomeUsuarioField().getText();
            String email = view.getEmailField().getText();
            String senha = new String(view.getSenhaField().getPassword());
            boolean admin = view.getAdminCheckBox().isSelected();
            boolean status = view.getStatusCheckBox().isSelected();

            if (nome.isEmpty() || nomeUsuario.isEmpty() || email.isEmpty() || senha.isEmpty()) {
                Notifications.getInstance().show(Notifications.Type.WARNING, Notifications.Location.TOP_CENTER, "Por favor, preencha todos os campos.");
                return;
            }

            if (!ValidationUtils.validarNome(nome)) {
                JOptionPane.showMessageDialog(view, "O nome não pode conter números.", "Erro", JOptionPane.ERROR_MESSAGE);
                return;
            }

            if (!ValidationUtils.validarEmail(email)) {
                JOptionPane.showMessageDialog(view, "O endereço de email não é válido.", "Erro", JOptionPane.ERROR_MESSAGE);
                return;
            }

            if (ValidationUtils.emailJaExisteUser(usuarioCrud.readAll(), email, -1)) {
                JOptionPane.showMessageDialog(view, "O email " + email + " já está em uso. Por favor, escolha outro.", "Erro", JOptionPane.ERROR_MESSAGE);
                return;
            }

            if (!nome.isEmpty() && !email.isEmpty() && !nomeUsuario.isEmpty()) {
                if (nomeUsuarioExiste(nomeUsuario, -1)) {
                    JOptionPane.showMessageDialog(view, "Nome de usuário " + nomeUsuario + " já existe. Escolha um diferente.", "Erro", JOptionPane.ERROR_MESSAGE);
                    return;
                }

                ultimoId++;
                Usuario novoUsuario = new Usuario(ultimoId, nome, nomeUsuario, email, senha, admin, status);
                usuarioCrud.create(novoUsuario);

                Notifications.getInstance().show(Notifications.Type.SUCCESS, Notifications.Location.TOP_CENTER, "Usuário registado com sucesso!");
                atualizarTabela();
                limparCampos();
            } else {
                JOptionPane.showMessageDialog(view, "Preencha todos os campos!");
            }
        }
    }

    // Listener para o botão de limpar
    class LimparListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            limparCampos();
        }
    }

    // Listener para o botão de carregar
    class CarregarListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            atualizarTabela();
        }
    }

    // Listener para o botão de atualizar
    class AtualizarListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            int selectedRow = view.getUsuariosTable().getSelectedRow();
            if (selectedRow >= 0) {
                int id = Integer.parseInt(view.getIdTextField().getText());
                String nome = view.getNomeField().getText();
                String nomeUsuario = view.getNomeUsuarioField().getText();
                String email = view.getEmailField().getText();
                String senha = new String(view.getSenhaField().getPassword());
                boolean admin = view.getAdminCheckBox().isSelected();
                boolean status = view.getStatusCheckBox().isSelected();

                if (!nome.isEmpty() && !email.isEmpty() && !nomeUsuario.isEmpty()) {
                    if (!ValidationUtils.validarNome(nome)) {
                        JOptionPane.showMessageDialog(view, "O nome não pode conter números.", "Erro", JOptionPane.ERROR_MESSAGE);
                        return;
                    }

                    if (!ValidationUtils.validarEmail(email)) {
                        JOptionPane.showMessageDialog(view, "O endereço de email não é válido.", "Erro", JOptionPane.ERROR_MESSAGE);
                        return;
                    }

                    if (nomeUsuarioExiste(nomeUsuario, id)) {
                        JOptionPane.showMessageDialog(view, "Nome de usuário já existe. Escolha um diferente.", "Erro", JOptionPane.ERROR_MESSAGE);
                        return;
                    }

                    // Preserva a senha atual se o campo estiver vazio
                    if (senha.isEmpty()) {
                        Usuario usuarioExistente = usuarioCrud.read(id);
                        senha = usuarioExistente.getSenha();
                    }

                    Usuario usuarioAtualizado = new Usuario(id, nome, nomeUsuario, email, senha, admin, status);
                    usuarioCrud.update(usuarioAtualizado);

                    atualizarTabela();
                    Notifications.getInstance().show(Notifications.Type.SUCCESS, Notifications.Location.TOP_CENTER, "Usuário " + nome + " actualizado com sucesso!");
                    limparCampos();
                } else {
                    JOptionPane.showMessageDialog(view, "Preencha todos os campos!");
                }
            } else {
                JOptionPane.showMessageDialog(view, "Selecione um usuário na tabela para actualizar.");
            }
        }
    }

    // Listener para o botão de eliminar
    class EliminarListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            int selectedRow = view.getUsuariosTable().getSelectedRow();
            if (selectedRow >= 0) {
                int id = (int) view.getUsuariosTable().getValueAt(selectedRow, 0);

                if (id == 1) {
                    Notifications.getInstance().show(Notifications.Type.WARNING, Notifications.Location.TOP_CENTER, "Este usuário nao pode ser eleminado");
                } else {

                    int confirmacao = JOptionPane.showConfirmDialog(null, "Deseja realmente eliminar este usuário", "Sim", JOptionPane.YES_NO_OPTION);
                    if (confirmacao == JOptionPane.YES_OPTION) {
                        usuarioCrud.delete(id);
                        Notifications.getInstance().show(Notifications.Type.SUCCESS, Notifications.Location.TOP_CENTER, "Usuário elimanado com sucesso!");
                    }

                }
                atualizarTabela();
                limparCampos();
            } else {
                JOptionPane.showMessageDialog(view, "Selecione um usuário na tabela para eliminar.");
            }
        }
    }

    // Listener para a seleção da tabela
    class SelecionarListener implements ListSelectionListener {
        @Override
        public void valueChanged(ListSelectionEvent e) {
            int selectedRow = view.getUsuariosTable().getSelectedRow();
            if (selectedRow >= 0) {
                int id = (int) view.getUsuariosTable().getValueAt(selectedRow, 0);
                String nome = (String) view.getUsuariosTable().getValueAt(selectedRow, 1);
                String nomeUsuario = (String) view.getUsuariosTable().getValueAt(selectedRow, 2);
                String email = (String) view.getUsuariosTable().getValueAt(selectedRow, 3);
                boolean admin = "Sim".equals(view.getUsuariosTable().getValueAt(selectedRow, 4));
                boolean status = "Activo".equals(view.getUsuariosTable().getValueAt(selectedRow, 5));

                view.getIdTextField().setText(String.valueOf(id));
                view.getNomeField().setText(nome);
                view.getNomeUsuarioField().setText(nomeUsuario);
                view.getEmailField().setText(email);
                view.getSenhaField().setText(""); // Senha não é carregada por segurança
                view.getAdminCheckBox().setSelected(admin);
                view.getStatusCheckBox().setSelected(status);
            }
        }
    }

    // Listener para o botão de pesquisar
    class PesquisarListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String criterio = view.getPesquisaField().getText().trim();
            if (!criterio.isEmpty()) {
                Predicate<Usuario> predicate = usuario ->
                        usuario.getNome().contains(criterio) ||
                                usuario.getNomeUsuario().contains(criterio) ||
                                usuario.getEmail().contains(criterio);

                List<Usuario> resultados = usuarioCrud.search(predicate);

                if (!resultados.isEmpty()) {
                    atualizarTabela(resultados);
                }else {
                    Notifications.getInstance().show(Notifications.Type.WARNING, Notifications.Location.TOP_CENTER, "Nenhum resultado encontrado.");
                }

            } else {
                Notifications.getInstance().show(Notifications.Type.WARNING, Notifications.Location.TOP_CENTER, "Por favor, insira um critério de pesquisa.");
            }
        }
    }

    // Atualiza a tabela de usuários com uma lista específica
    public void atualizarTabela(List<Usuario> usuarios) {
        DefaultTableModel tableModel = (DefaultTableModel) view.getUsuariosTable().getModel();
        tableModel.setRowCount(0); // Limpa a tabela antes de adicionar os novos dados

        for (Usuario u : usuarios) {
            tableModel.addRow(new Object[]{
                    u.getId(),
                    u.getNome(),
                    u.getNomeUsuario(),
                    u.getEmail(),
                    u.isAdmin() ? "Sim" : "Não",
                    u.getStatus() ? "Activo" : "Inativo"
            });
        }
    }

    // Atualiza a tabela de usuários com todos os dados
    public void atualizarTabela() {
        atualizarTabela(usuarioCrud.readAll());
    }

    // Limpa os campos do painel
    private void limparCampos() {
        view.getIdTextField().setText("");
        view.getNomeField().setText("");
        view.getNomeUsuarioField().setText("");
        view.getEmailField().setText("");
        view.getSenhaField().setText("");
        view.getAdminCheckBox().setSelected(true);
        view.getStatusCheckBox().setSelected(true);
        view.getPesquisaField().setText("");
    }
}
