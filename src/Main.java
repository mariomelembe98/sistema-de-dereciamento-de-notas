import DAO.*;
import app.models.Estudante;
import app.models.Professor;
import app.models.Usuario;
import com.formdev.flatlaf.FlatIntelliJLaf;
import raven.toast.Notifications;
import app.services.LoginService;
import views.TelaLogin;
import views.dashboard.TelaPrincipal;

import javax.swing.*;
import java.util.List;

public class Main {
    private CrudDeUsuarios crudDeUsuarios;
    private CrudDeProfessores crudDeProfessores;
    private CrudDeEstudantes crudDeEstudantes;
    private CrudDeCursos crudDeCursos;

    public Main() {
        // Inicializar CRUDs
        crudDeUsuarios = new CrudDeUsuarios("usuarios.dat");
        crudDeProfessores = new CrudDeProfessores("professores.dat");
        crudDeEstudantes = new CrudDeEstudantes("estudantes.dat");
        crudDeCursos = new CrudDeCursos("cursos.dat");

        // Mostrar tela de login ao iniciar
        exibirTelaLogin();
    }

    private void exibirTelaLogin() {
        List<Usuario> usuarios = crudDeUsuarios.readAll();
        List<Professor> professores = crudDeProfessores.readAll();
        List<Estudante> estudantes = crudDeEstudantes.readAll();

        LoginService loginService = new LoginService(usuarios, professores, estudantes);

        TelaLogin telaLogin = new TelaLogin(loginService);
        telaLogin.setLoginListener(new LoginListener() {
            @Override
            public void onLoginSuccess() {
                exibirTelaPrincipal();
            }

            @Override
            public void onLoginFailed() {
                Notifications.getInstance().show(Notifications.Type.WARNING, Notifications.Location.TOP_CENTER, "Credenciais inválidas. Tente novamente.");
            }
        });
        telaLogin.exibir();
    }

    private void exibirTelaPrincipal() {
        TelaPrincipal telaPrincipal = new TelaPrincipal();
        telaPrincipal.exibir();
    }

    public static void main(String[] args) {
        // Instalar o FlatLaf look and feel
        FlatIntelliJLaf.setup();
        SwingUtilities.invokeLater(Main::new);
    }
}
